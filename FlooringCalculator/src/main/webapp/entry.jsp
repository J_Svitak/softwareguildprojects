<%-- 
    Document   : entry
    Created on : May 27, 2016, 2:53:14 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <style>
            div{
                width: 500px;
                text-align: right;
            }
            input {
                width: 120px;
            }
            select {
                width: 120px;
            }
            #container {
                text-align: center;
                width: 99%;
                border: 1px solid navy;
            }
        </style>
        
        <title>Flooring Calc Entry</title>
    </head>
    <body>
        <form method="POST" action="FlooringCalculatorServlet">
            <div id="container">
                <h1>Welcome to the Flooring Calculator!</h1>
                <h3>Complete the fields below</h3>
                <div>
                    Width
                    <input type="text" name="width"/>
                    ${errorWidth}
                </div>
                <div>
                    Length
                    <input type="text" name="length" />
                    ${errorLength}
                </div>
                <div>
                    Cost Per Sq Ft
                    <input type="text" name="costPerSqFt" />
                    ${errorMaterialCost}
                </div>
                <div>
                    <input type="submit" value="Calculate" />
                </div>
            </div>
        </form>
    </body>
</html>
