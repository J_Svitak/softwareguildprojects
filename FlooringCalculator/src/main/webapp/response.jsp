<%-- 
    Document   : response
    Created on : May 27, 2016, 2:53:28 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            div{
                width: 500px;
                text-align: right;
            }
            input {
                width: 120px;
            }
            select {
                width: 120px;
            }
            #container {
                text-align: center;
                width: 99%;
                border: 1px solid navy;
            }
        </style>
        
        <title>JSP Page</title>
    </head>
    <body>
        <div id="container">
            <h1>Flooring Calculator Results</h1>
            <div>                
                <div>
                    Width: ${width}
                </div>
                <div>
                    Length ${length}
                </div>
                <div>
                    Area: ${area}
                </div>
                <div>
                    Cost Per Sq Ft: ${costPerSqFt}
                </div>
                <div>
                    Material Cost: ${materialCost}
                </div>
                <div>
                    Labor Cost Per Hour ${laborPerHour}
                </div>
                <div>
                    Total Labor Cost ${laborCost}
                </div>
                <div>
                    Order Total: ${totalCost}
                </div>
            </div>            
        </div>
    </body>
</html>
