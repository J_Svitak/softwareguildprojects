/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(urlPatterns = {"/FlooringCalculatorServlet"})
public class FlooringCalculatorServlet extends HttpServlet {

    private float width;
    private float length;
    private float costPerSqFt;
    private float materialCost;
    private float laborCostPerHour; 
    private float totalLaborCost;
    private float totalCost;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        RequestDispatcher dispatchEntry = request.getRequestDispatcher("entry.jsp");
        dispatchEntry.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        this.width = 0f;
        this.length = 0f;
        this.costPerSqFt = 0f;
        this.materialCost = 0f;
        this.laborCostPerHour = 86.0f; 
        this.totalLaborCost = 0f;
        this.totalCost = 0f;
        
        boolean isValid = formEntry(request);

        if (!isValid) {
            RequestDispatcher dispatchEntry = request.getRequestDispatcher("entry.jsp");
            dispatchEntry.forward(request, response);
        }        
        
        Float area = this.length * this.width;
        this.materialCost = costPerSqFt * area;
        
        Float estimatedTime = area / 20;
        this.totalLaborCost = estimatedTime * this.laborCostPerHour;
        
        this.totalCost = this.materialCost + this.totalLaborCost;
        
        request.setAttribute("width", this.width);
        request.setAttribute("length", this.length);
        request.setAttribute("area", area);
        request.setAttribute("costPerSqFt", this.costPerSqFt);
        request.setAttribute("materialCost", this.materialCost);
        request.setAttribute("laborPerHour", this.laborCostPerHour);
        request.setAttribute("laborCost", this.totalLaborCost);
        request.setAttribute("totalCost", this.totalCost);
        
        RequestDispatcher dispatchResponse = request.getRequestDispatcher("response.jsp");
        dispatchResponse.forward(request, response);
    }
    
    private boolean formEntry(HttpServletRequest request) {
        
        boolean isValid = true;
        
        try {
            this.width = Float.parseFloat(request.getParameter("width"));            
        }
        catch (NumberFormatException ex) {
            request.setAttribute("errorInterestRate", "Your entry was not a valid number!");            
            isValid = false;
        }

        try {
            this.length = Float.parseFloat(request.getParameter("length"));            
        }
        catch (NumberFormatException ex) {
            request.setAttribute("errorInterestRate", "Your entry was not a valid number!");            
            isValid = false;
        }
        
        try {
            this.costPerSqFt = Float.parseFloat(request.getParameter("costPerSqFt"));            
        }
        catch (NumberFormatException ex) {
            request.setAttribute("errorInterestRate", "Your entry was not a valid number!");            
            isValid = false;
        }
        
        return isValid;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
