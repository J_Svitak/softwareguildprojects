/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(urlPatterns = {"/LuckySevensServlet"})
public class LuckySevensServlet extends HttpServlet {

    private int totalAmount;
    private int highAmount;
    private int rollCount;
    private int highRollCount;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        RequestDispatcher rd = request.getRequestDispatcher("entry.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        int startingAmount = 0;
        
        try {
            startingAmount = Integer.parseInt(request.getParameter("startingAmount"));            
        }
        catch (NumberFormatException ex) {
            request.setAttribute("errorMessage", "Your entry was not a valid number!");
            RequestDispatcher rdEntry = request.getRequestDispatcher("entry.jsp");
            rdEntry.forward(request, response);
            return;
        }
        
        totalAmount = startingAmount;
        
        highAmount = totalAmount;
        rollCount = 0;
        highRollCount = 0;

        while (totalAmount > 0) {
            int diceSum = rollDice();
            rollCount++;
            //this.rollCount = rollCount++;
            
            updateGameInfo(diceSum);
            
            totalAmount = getTotalAmount();
            highAmount = getHighAmount();
            highRollCount = getHighRollCount();            
        }
        
        request.setAttribute("rollCount", rollCount);
        request.setAttribute("highAmount", highAmount);
        request.setAttribute("highRollCount", highRollCount);
        
        RequestDispatcher rdResponse = request.getRequestDispatcher("response.jsp");
        rdResponse.forward(request, response);
    }
    
    public int rollDice() {
    
        //roll the dice (two random numbers between on and six)
        int dice1 = (int)(Math.random() * (6 + 1));
        int dice2 = (int)(Math.random() * (6 + 1));
        //add the two dice together
        int diceSum = dice1 + dice2;    

        return diceSum;
    }
    
    public void updateGameInfo(int diceSum) {//, int totalAmount, int highAmount, int rollCount) {
        
        int newAmount = this.totalAmount;
        
        if (diceSum == 7) {
            newAmount += 4;

            if (newAmount >= this.highAmount) {
                //highAmount = totalAmount;                
                //highRollCount = rollCount;
                //setHighAmount(totalAmount);
                //setHighRollCount(rollCount);
                this.highAmount = newAmount;
                this.highRollCount = rollCount;
            }
        }
        else {
            newAmount--;
        }
        
        this.totalAmount = newAmount;
        //setTotalAmount(totalAmount);
        System.out.println(rollCount + ": Total Amount: " + totalAmount);        
    }
    
    public int getTotalAmount() {
        return this.totalAmount;
    }    
    
    public int getHighAmount() {
        return this.highAmount;
    }    
    
    public int getHighRollCount() {
        return this.highRollCount;
    }

    public String getName() {
        return "Lucky Sevens";
    }    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
