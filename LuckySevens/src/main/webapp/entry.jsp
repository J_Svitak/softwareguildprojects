<%-- 
    Document   : entry
    Created on : May 26, 2016, 3:52:05 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>        
        <form method="post" action="LuckySevensServlet">
            <h1>Welcome to Lucky Sevens!</h1>
            Please Enter the amount you would like to play with.<br/>
            <input type="text" name="startingAmount" />
            <input type="submit" value="Start Game" /> <br/>
            <br/>
            <h3>${errorMessage}</h3>
            
        </form>
    </body>
</html>
