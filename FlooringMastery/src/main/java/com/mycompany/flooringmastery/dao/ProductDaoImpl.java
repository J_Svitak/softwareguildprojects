/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Product;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ProductDaoImpl implements ProductDao {
    
    private List<Product> productList;
    private boolean isTest;
    private final String filePath = "/home/apprentice/apache-tomcat-8.0.35/bin/FlooringMasteryData/Products.txt";
    
    public ProductDaoImpl() {
        productList = new ArrayList();
        decode();
    }
    
    @Override
    public void setIsTest(boolean isTest) {
        this.isTest = isTest;
    }
    
    @Override
    public Product create(Product objProduct) {
        
        productList.add(objProduct);
        encode();
        
        return objProduct;
    }
    
    @Override
    public Product get(String productType) {
        
        for (Product objProduct : productList) {
            if (productType.equals(objProduct.getProductType())) {
                return objProduct;
            }
        }        
        return null;
    }
    
    
    
    @Override
    public Product update(Product objProduct) {
        
        for (Product myProduct : productList) {
            if (myProduct.getProductType().equals(objProduct.getProductType())) {
                productList.remove(myProduct);
                productList.add(objProduct);
                break;
            }
        }
        
        encode();
        return objProduct;
    }
        
    @Override
    public Product delete(Product objProduct) {
        for (Product myProduct : productList) {
            if (myProduct.getProductType().equals(objProduct.getProductType())) {
                productList.remove(myProduct);                
                break;
            }
        }
        
        encode();
        return objProduct;
    }
    
    @Override
    public List<Product> getProductList() {
        return productList;
    }    
    
    @Override
    public void encode() {
        
        final String TOKEN = ",";
        
        try {            
            PrintWriter out = new PrintWriter(new FileWriter(filePath));
            
            out.println("ProductType,CostPerSqFt,LaborCostPerSqFt");
            
            for (Product objProduct : productList) {                
                
                out.print(objProduct.getProductType());
                out.print(TOKEN);
                
                out.print(objProduct.getCostPerSqFt());
                out.print(TOKEN);
                
                out.print(objProduct.getLaborCostperSqFt());                
                out.println();
            }            
            
            out.flush();
            out.close();
        } 
        catch (IOException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
        
    @Override
    public List<Product> decode() {
                
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(filePath)));
            
            int lineNumber = 0;
            
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                
                lineNumber++;                
                if (lineNumber == 1) {
                    continue;
                }
                
                String[] stringParts = currentLine.split(",");                
                
                Product objProduct = new Product();
                
                String productType = stringParts[0];
                double costPerSqFt = Double.parseDouble(stringParts[1]);
                double laborCostPerSqFt = Double.parseDouble(stringParts[2]);                
                
                objProduct.setProductType(productType);
                objProduct.setCostPerSqFt(costPerSqFt);
                objProduct.setLaborCostperSqFt(laborCostPerSqFt);                
                                
                productList.add(objProduct);
            }
        } 
        catch (FileNotFoundException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return productList;
    }
}
