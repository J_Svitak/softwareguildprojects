/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Tax;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TaxDaoImpl implements TaxDao {
    
    private List<Tax> taxList;
    private boolean isTest;
    private final String filePath = "/home/apprentice/SelfWork/Labs/FlooringMastery/DataFiles/Taxes.txt";
    
    public TaxDaoImpl() {
        taxList = new ArrayList();
        decode();
    }
    
    @Override
    public void setIsTest(boolean isTest) {
        this.isTest = isTest;
    }
    
    @Override
    public Tax create(Tax objTax) {
        
        taxList.add(objTax);
        encode();
        
        return objTax;
    }
    
    @Override
    public Tax get(String state) {
        
        for (Tax objTax : taxList) {
            if (state.equals(objTax.getState())) {
                return objTax;
            }
        }        
        return null;
    }
    
    @Override
    public Tax update(Tax objTax) {
        
        for (Tax myTax : taxList) {
            if (myTax.getState().equals(objTax.getState())) {
                taxList.remove(myTax);
                taxList.add(objTax);
                break;
            }
        }
        
        encode();
        return objTax;
    }
        
    @Override
    public Tax delete(Tax objTax) {
        for (Tax myTax : taxList) {
            if (myTax.getState().equals(objTax.getState())) {
                taxList.remove(myTax);                
                break;
            }
        }
        
        encode();
        return objTax;
    }
    
    @Override
    public void encode() {
        
        final String TOKEN = ",";
        
        try {            
            PrintWriter out = new PrintWriter(new FileWriter(filePath));
            
            out.println("State,TaxRate");
            
            for (Tax objTax : taxList) {                
                
                out.print(objTax.getState());
                out.print(TOKEN);
                
                out.print(objTax.getTaxRate());                
                out.println();
            }            
            
            out.flush();
            out.close();
        } 
        catch (IOException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    @Override
    public List<Tax> getTaxList() {
        return taxList;
    }
        
    public List<Tax> decode() {
                
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(filePath)));
            
            int lineNumber = 0;
            
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                
                lineNumber++;                
                if (lineNumber == 1) {
                    continue;
                }
                
                String[] stringParts = currentLine.split(",");                
                
                Tax objTax = new Tax();
                
                String state = stringParts[0];
                double taxRate = Double.parseDouble(stringParts[1]);                               
                
                objTax.setState(state);
                objTax.setTaxRate(taxRate);                
                                
                taxList.add(objTax);
            }
        } 
        catch (FileNotFoundException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return taxList;
    }
}
