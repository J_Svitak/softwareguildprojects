/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Order;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class OrderDaoImpl implements OrderDao {
    
    private int nextOrderNumber = 101;
    private Map<String, List<Order>> orderMap;
    private boolean isTest;
    private final String directory = "/home/apprentice/apache-tomcat-8.0.35/bin/FlooringMasteryData/Orders/";
    
    public OrderDaoImpl() {
        orderMap = new HashMap();
        orderMap = decode();
    }
    
    @Override
    public void setIsTest(boolean isTest) {
        this.isTest = false;
    }
    
    @Override
    public Order create(Order objOrder) {
        objOrder.setOrderNumber(nextOrderNumber);
        nextOrderNumber++;
        String dt = "";
        
        /*SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy");
        if (objOrder.getOrderDate().equals("") || objOrder.getOrderDate() == null) {
            Calendar c1 = Calendar.getInstance();
            dt = formatter.format(c1.getTime());
        }
        else {
            dt = objOrder.getOrderDate();
        }*/
        
        dt = objOrder.getOrderDate();
        
        objOrder.setOrderDate(dt);
                
        List<Order> orderList = new ArrayList();
        
        if (orderMap.containsKey(dt)) {            
            orderList = orderMap.get(dt);
        }
        
        orderList.add(objOrder);
        orderMap.put(dt, orderList);        
                
        if (!isTest) {
            encode();
        }
        
        return objOrder;
    }    
    
    @Override
    public Order get(int orderNumber) {
        
        Set<String> orderSet = orderMap.keySet();
        
        for (String dt : orderSet) {
            List <Order> orderList = orderMap.get(dt);
            
            for (Order objOrder : orderList) {
                if (orderNumber == objOrder.getOrderNumber()) {
                    return objOrder;
                }
            }
        }
        return null;
    }
    
    @Override
    public Order getByCustomer(String customerName) {
        
        Set<String> orderSet = orderMap.keySet();
        
        for (String dt : orderSet) {
            List <Order> orderList = orderMap.get(dt);
            
            for (Order objOrder : orderList) {
                if (objOrder.getCustomerName().equals(customerName)) {
                    return objOrder;
                }
            }
        }
        return null;
    }
    
    @Override
    public List<Order> getByOrderDate(String orderDate) {
        List<Order> ordersByDate = new ArrayList();
        
        for (Order objOrder : getOrderList()) {
            if (objOrder.getOrderDate().equals(orderDate)) {
                ordersByDate.add(objOrder);
            }
        }
        
        return ordersByDate;
    }
    
    @Override
    public Order update(Order objOriginalOrder, Order objNewOrder) {
        
        //Set<String> orderSet = orderMap.keySet();
        List<Order> orderListForOriginal = orderMap.get(objOriginalOrder.getOrderDate());        
        
        for (Order myOrder : orderListForOriginal) {
            if (myOrder.getOrderNumber()== objOriginalOrder.getOrderNumber()) {
                orderListForOriginal.remove(myOrder);
                break;
            }
        }
        
        List <Order> orderListForUdpate = new ArrayList();
        
        if (orderMap.containsKey(objNewOrder.getOrderDate())) {            
            orderListForUdpate = orderMap.get(objNewOrder.getOrderDate());
        }
        
        orderListForUdpate.add(objNewOrder);
        orderMap.put(objNewOrder.getOrderDate(), orderListForUdpate);        
        
        if (!isTest) {
            encode();
        }
        
        return objNewOrder;
    }
    
    @Override
    public Order clone(Order objOrder) {
        
        Order newOrder = new Order();
        
        newOrder.setOrderNumber(objOrder.getOrderNumber());
        newOrder.setOrderDate(objOrder.getOrderDate());
        newOrder.setCustomerName(objOrder.getCustomerName());
        newOrder.setState(objOrder.getState());
        newOrder.setTaxRate(objOrder.getTaxRate());
        newOrder.setProductType(objOrder.getProductType());
        newOrder.setArea(objOrder.getArea());
        newOrder.setCostPerSqFt(objOrder.getCostPerSqFt());
        newOrder.setLaborCostPerSqFt(objOrder.getLaborCostPerSqFt());
        newOrder.setMaterialCost(objOrder.getMaterialCost());
        newOrder.setTotalLaborCost(objOrder.getTotalLaborCost());
        newOrder.setTax(objOrder.getTax());
        newOrder.setOrderTotal(objOrder.getOrderTotal());
        
        return newOrder;
    }
        
    @Override
    public Order delete(Order objOrder) {
        
        Set<String> orderSet = orderMap.keySet();
        
        for (String dt : orderSet) {
            
            List <Order> orderList = orderMap.get(dt);
            for (Order myOrder : orderList) {
                if (myOrder.getOrderNumber()== objOrder.getOrderNumber()) {
                    orderList.remove(myOrder);                
                    break;
                }
            }
        }
        
        if (!isTest) {
            encode();
        }
        
        return objOrder;
    }
    
    @Override
    public List<Order> getOrderList() {
        
        List <Order> orderList = new ArrayList();
        Set<String> orderSet = orderMap.keySet();
        
        for (String dt : orderSet) {            
            for (Order objOrder : orderMap.get(dt)) {
                orderList.add(objOrder);
            }
        }
        
        return orderList;
    }
    
    @Override
    public void encode() {
        
        final String TOKEN = ",";
        
        Set<String> orderSet = orderMap.keySet();
        
        for (String dt : orderSet) {
            List <Order> orderList = orderMap.get(dt);
            String filename = String.format("%sOrders_%s.txt", directory, dt);
            
            try {            
                PrintWriter out = new PrintWriter(new FileWriter(filename));

                out.println("OrderNumber,CustomerName,State,TaxRate,ProductType,Area,CostPerSquareFoot,"
                        + "LaborCostPerSquareFoot,MaterialCost,LaborCost,Tax,Total");

                for (Order objOrder : orderList) {
                    
                    encodeWithToken(out, String.valueOf(objOrder.getOrderNumber()), TOKEN);                    
                    encodeWithToken(out, objOrder.getCustomerName(), TOKEN);
                    encodeWithToken(out, objOrder.getState(), TOKEN);                    
                    encodeWithToken(out, String.valueOf(objOrder.getTaxRate()), TOKEN);                                        
                    encodeWithToken(out, objOrder.getProductType(), TOKEN);                    
                    encodeWithToken(out, String.valueOf(objOrder.getArea()), TOKEN);
                    encodeWithToken(out, String.valueOf(objOrder.getCostPerSqFt()), TOKEN);
                    encodeWithToken(out, String.valueOf(objOrder.getLaborCostPerSqFt()), TOKEN);
                    encodeWithToken(out, String.valueOf(objOrder.getMaterialCost()), TOKEN);
                    encodeWithToken(out, String.valueOf(objOrder.getTotalLaborCost()), TOKEN);
                    encodeWithToken(out, String.valueOf(objOrder.getTax()), TOKEN);
                    double testTotal = objOrder.getOrderTotal();
                    encodeWithToken(out, String.valueOf(objOrder.getOrderTotal()), "");
                    
                    out.println();
                }            

                out.flush();
                out.close();
            } 
            catch (IOException ex) {
                //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void encodeWithToken(PrintWriter out, String stringToEncode, String token) {
        
        String testString = stringToEncode;
        if (!token.equals("")) {
            if (stringToEncode.contains(token)) {
                stringToEncode = stringToEncode.replace(token, "\\,");
            }
        }
        
        out.print(stringToEncode);
        out.print(token);
    }
    
    @Override    
    public Map<String, List<Order>> decode() {
                
        try {
            File folder = new File(directory);
            
            File[] fileList = folder.listFiles();
            for (File file : fileList){
                if (file.isFile()){
                    String filename = file.getName();
                    int subEnd = filename.indexOf(".txt");
                    String dt = filename.substring(7, subEnd);
                    
                    Scanner sc = new Scanner(new BufferedReader(new FileReader(file)));                    

                    List<Order> orderList = new ArrayList();

                    int lineNumber = 0;

                    while (sc.hasNextLine()) {
                        String currentLine = sc.nextLine();                        

                        lineNumber++;                
                        if (lineNumber == 1) {
                            continue;
                        }                        

                        String[] stringParts = currentLine.split(",");                

                        Order objOrder = new Order();
                        
                        int i = 0;
                        boolean isContinue = false;
                        
                        for (String strPart : stringParts) {
                            if (isContinue) {
                                isContinue = false;
                                continue;
                            }
                            
                            if (stringParts[i].endsWith("\\")) {
                                strPart = stringParts[i] + stringParts[i + 1];
                                strPart = strPart.replace("\\", ",");
                                isContinue = true;
                            }
                            
                            String parameterName = getProperty(i);
                            i++;
                            
                            switch (parameterName) {
                                case "orderNumber":
                                    objOrder.setOrderNumber(Integer.parseInt(strPart));
                                    break;
                                case "customerName":
                                    objOrder.setCustomerName(strPart);
                                    break;
                                case "state":
                                    objOrder.setState(strPart);
                                    break;
                                case "taxRate":
                                    objOrder.setTaxRate(Double.parseDouble(strPart));
                                    break;
                                case "productType":
                                    objOrder.setProductType(strPart);
                                    break;
                                case "area":
                                    objOrder.setArea(Double.parseDouble(strPart));
                                    break;
                                case "costPerSqFt":
                                    objOrder.setCostPerSqFt(Double.parseDouble(strPart));
                                    break;
                                case "laborCostPerSqFt":
                                    objOrder.setLaborCostPerSqFt(Double.parseDouble(strPart));
                                    break;
                                case "materialCost":
                                    objOrder.setMaterialCost(Double.parseDouble(strPart));
                                    break;
                                case "totalLaborCost":
                                    objOrder.setTotalLaborCost(Double.parseDouble(strPart));
                                    break;
                                case "tax":
                                    objOrder.setTax(Double.parseDouble(strPart));
                                    break;
                                case "orderTotal":
                                    objOrder.setOrderTotal(Double.parseDouble(strPart));
                                    break;
                            }
                        }
                        
                        int orderNum = objOrder.getOrderNumber();

                        objOrder.setOrderNumber(orderNum);
                        objOrder.setOrderDate(dt);

                        orderList.add(objOrder);

                        if (orderNum >= nextOrderNumber) {
                            nextOrderNumber = orderNum + 1;
                        }
                    }

                    orderMap.put(dt, orderList);
                }
            }
        } 
        catch (FileNotFoundException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return orderMap;
    }
    
    private String getProperty(int index) {
        
        Map<Integer, String> decodingMap = new HashMap();
        decodingMap.put(0, "orderNumber");
        decodingMap.put(1, "customerName");
        decodingMap.put(2, "state");
        decodingMap.put(3, "taxRate");
        decodingMap.put(4, "productType");
        decodingMap.put(5, "area");
        decodingMap.put(6, "costPerSqFt");
        decodingMap.put(7, "laborCostPerSqFt");
        decodingMap.put(8, "materialCost");
        decodingMap.put(9, "totalLaborCost");
        decodingMap.put(10, "tax");
        decodingMap.put(11, "orderTotal");
        
        String returnVal = decodingMap.get(index);
        
        return returnVal;
    }
}
