/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Order;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface OrderDao {
    
    public void setIsTest(boolean isTest);
    public Order create(Order objOrder);
    public Order get(int orderNumber);
    public Order getByCustomer(String customerName);
    public List<Order> getByOrderDate(String orderDate);
    public Order update(Order objOriginalOrder, Order objOrder);
    public Order delete(Order objOrder);
    public Order clone(Order objOrder);
    public List<Order> getOrderList();
    public void encode();
    public Map<String, List<Order>> decode();
}
