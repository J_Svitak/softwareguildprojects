/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Product;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface ProductDao {
    
    public void setIsTest(boolean isTest);
    public Product create(Product objProduct);
    public Product get(String productType);
    public Product update(Product objProduct);
    public Product delete(Product objProduct);
    public List<Product> getProductList();
    public void encode();
    public List<Product> decode();
}
