/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dao;

import com.mycompany.flooringmastery.dto.Tax;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface TaxDao {
    
    public void setIsTest(boolean isTest);
    public Tax create(Tax objTax);
    public Tax get(String state);
    public Tax update(Tax objTax);
    public Tax delete(Tax objTax);
    public List<Tax> getTaxList();
    public void encode();
    public List<Tax> decode();
}
