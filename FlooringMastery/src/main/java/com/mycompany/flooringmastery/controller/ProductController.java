/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.controller;


import com.mycompany.flooringmastery.dao.ProductDao;
import com.mycompany.flooringmastery.dto.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/product")
public class ProductController {
    
    private ProductDao productDao;
    private List<Product> productList = new ArrayList();
    
    @Inject
    public ProductController(ProductDao dao) {
        this.productDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.POST)
    @ResponseBody
    public Product create(@Valid @RequestBody Product product, Map model) {
        
        return productDao.create(product);        
    }
    
    @RequestMapping(value="/{productType}", method=RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("productType") String productType) {
        Product objProduct = productDao.get(productType);
        productDao.delete(objProduct);        
    }
    
    @RequestMapping(value="/edit/{productType}", method=RequestMethod.GET)
    public String edit(@PathVariable("productType") String productType, Map model) {
        
        Product objProduct = productDao.get(productType);
        model.put("product", objProduct);        
        return "editproduct";
    }
    
    @RequestMapping(value="/", method=RequestMethod.PUT)
    @ResponseBody
    public Product editSubmit(@RequestBody Product objProduct) {
        
        return productDao.update(objProduct);
    }
    
    @RequestMapping(value="/{productType}", method=RequestMethod.GET)
    @ResponseBody
    public Product show(@PathVariable("productType") String productType) {
        Product objProduct = productDao.get(productType);
        return objProduct;
    }

@RequestMapping(value="/filter", method=RequestMethod.POST)
    public String filterAddressList(@RequestParam("productFilterValue") String filterValue, Map model) {
        
        productList = new ArrayList();
         
        Product objProduct = productDao.get(filterValue);
                
        if (objProduct != null) {
            productList.add(objProduct);
        }        
        
        boolean isProductListEmpty = true;
        if (productList.size() > 0) {
            isProductListEmpty = false;
        }
        
        model.put("productList", productList);
        model.put("isProductListEmpty", isProductListEmpty);
        
        return "admin";
    }    
}
