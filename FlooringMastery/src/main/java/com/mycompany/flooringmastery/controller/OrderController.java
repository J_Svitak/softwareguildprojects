/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.controller;

import com.mycompany.flooringmastery.dao.OrderDao;
import com.mycompany.flooringmastery.dao.ProductDao;
import com.mycompany.flooringmastery.dao.TaxDao;
import com.mycompany.flooringmastery.dto.Order;
import com.mycompany.flooringmastery.dto.Product;
import com.mycompany.flooringmastery.dto.Tax;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/order")
public class OrderController {
    
    private OrderDao orderDao;
    private ProductDao productDao;
    private TaxDao taxDao;
    List<Order> orderList = new ArrayList();
    private NumberFormat formatter;
    
    @Inject
    public OrderController(OrderDao orderDao, ProductDao productDao, TaxDao taxDao) {
        this.orderDao = orderDao;
        this.productDao = productDao;
        this.taxDao = taxDao;
        formatter = new DecimalFormat("#0.00");
    }
    
    @RequestMapping(value="/", method=RequestMethod.POST)
    @ResponseBody
    public Order create(@Valid @RequestBody Order order) {
                
        Tax selectedTax = null;
        Product selectedProduct = null;
        
        selectedTax = taxDao.get(order.getState());
        selectedProduct = productDao.get(order.getProductType());
        
        calculateCost(order, selectedTax, selectedProduct);
        return orderDao.create(order);        
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer orderNumber) {
        Order objOrder = orderDao.get(orderNumber);
        orderDao.delete(objOrder);        
    }
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public String edit(@PathVariable("id") Integer orderNumber, Map model) {
        
        Order objOrder = orderDao.get(orderNumber);
        model.put("order", objOrder);        
        return "editorder";
    }
    
    @RequestMapping(value="/", method=RequestMethod.PUT)
    @ResponseBody
    public Order editSubmit(@Valid @RequestBody Order objOrder) {
        
        Tax selectedTax = null;
        Product selectedProduct = null;
        
        selectedTax = taxDao.get(objOrder.getState());
        selectedProduct = productDao.get(objOrder.getProductType());
        
        calculateCost(objOrder, selectedTax, selectedProduct);
        
        Order updatedOrder = orderDao.update(orderDao.get(objOrder.getOrderNumber()), objOrder);
        return updatedOrder;        
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Order show(@PathVariable("id") Integer orderNumber) {
        
        Order objOrder = orderDao.get(orderNumber);        
        return objOrder;
    }
    
    @RequestMapping(value="/filter/{value}", method=RequestMethod.POST)
    @ResponseBody    
    public List<Order> filterOrderList(@PathVariable("value") String filterValue) {
        
        orderList = new ArrayList();
        Order objOrder = null;
        
        try {
            int orderNum = Integer.parseInt(filterValue);
            objOrder = orderDao.get(Integer.parseInt(filterValue));
        }
        catch (NumberFormatException ex) {
            
        }        

        if (objOrder != null) {
            orderList.add(objOrder);
        }
        
        objOrder = null;
        objOrder = orderDao.getByCustomer(filterValue);
        
        if (objOrder != null) {
            orderList.add(objOrder);
        }
        
        List<Order> orderListByDate = new ArrayList();        
        orderListByDate = orderDao.getByOrderDate(filterValue);
        
        for (Order order : orderListByDate) {
            orderList.add(order);
        }
        
        return orderList;
    }
    
    @RequestMapping(value="/orderlist", method=RequestMethod.POST)
    @ResponseBody    
    public List<Order> clearFilter() {
        return orderDao.getOrderList();
    }
    
    public void calculateCost(Order objOrder, Tax objTax, Product objProduct) {
        double materialCost = 0;
        double totalLaborCost = 0;
        
        if (objOrder != null && objProduct != null) {
            //set cost rate/amount        
            double costPerSqFt = Double.parseDouble(formatter.format(objProduct.getCostPerSqFt()));
            materialCost = Double.parseDouble(formatter.format(objOrder.getArea() * costPerSqFt));
            objOrder.setCostPerSqFt(costPerSqFt);
            objOrder.setMaterialCost(materialCost);
                
            //set labor cost rate/amount
            double laborCostPerSqFt = Double.parseDouble(formatter.format(objProduct.getLaborCostperSqFt()));
            totalLaborCost = Double.parseDouble(formatter.format(objOrder.getArea() * laborCostPerSqFt));
            objOrder.setLaborCostPerSqFt(laborCostPerSqFt);
            objOrder.setTotalLaborCost(totalLaborCost);
        }
        
        if (objOrder != null && objTax != null) {
            //set tax rate/amount
            double taxRate = objTax.getTaxRate();
            objOrder.setTaxRate(taxRate);

            taxRate = taxRate / 100;
            double taxAmount = Double.parseDouble(formatter.format(taxRate * (materialCost + totalLaborCost)));
            objOrder.setTax(Double.parseDouble(formatter.format(taxAmount)));
            objOrder.setOrderTotal(Double.parseDouble(formatter.format(materialCost + totalLaborCost + taxAmount)));
        }
    }
}
