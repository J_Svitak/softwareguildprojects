package com.mycompany.flooringmastery.controller;

import com.mycompany.flooringmastery.dao.OrderDao;
import com.mycompany.flooringmastery.dao.ProductDao;
import com.mycompany.flooringmastery.dao.TaxDao;
import com.mycompany.flooringmastery.dto.Order;
import com.mycompany.flooringmastery.dto.Product;
import com.mycompany.flooringmastery.dto.Tax;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
    
    private OrderDao orderDao;
    private ProductDao productDao;
    private TaxDao taxDao;
    private List<Order> orderList = new ArrayList();
    private List<Product> productList = new ArrayList();
    private List<Tax> taxList = new ArrayList();
    
    @Inject
    public HomeController(OrderDao orderDao, ProductDao productDao, TaxDao taxDao) {
        this.orderDao = orderDao;
        this.productDao = productDao;
        this.taxDao = taxDao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String loadFlooringMastery(Map<String, Object> model) {
        
        orderList = orderDao.getOrderList();
        boolean isOrderListEmpty = true;
        
        if (orderList.size() > 0) {
            isOrderListEmpty = false;
        }
        
        model.put("orderList", orderList );
        model.put("isOrderListEmpty", isOrderListEmpty);
        
        productList = productDao.getProductList();
        boolean isProductListEmpty = true;
        
        if (productList.size() > 0) {
            isProductListEmpty = false;
        }
        
        model.put("productList", productList);
        model.put("isOrderListEmpty", isProductListEmpty);
        
        taxList = taxDao.getTaxList();
        boolean isTaxListEmpty = true;
        
        if (taxList.size() > 0) {
            isTaxListEmpty = false;
        }
        
        model.put("taxList", taxList);
        model.put("isTaxListEmpty", isTaxListEmpty);
        
        return "home";
    }
    
    @RequestMapping(value="/admin", method=RequestMethod.GET)
    public String loadAdminPage(Map<String, Object> model) {
        
        productList = productDao.getProductList();
        boolean isProductListEmpty = true;
        
        if (productList.size() > 0) {
            isProductListEmpty = false;
        }
        
        model.put("productList", productList);
        model.put("isOrderListEmpty", isProductListEmpty);
        
        taxList = taxDao.getTaxList();
        boolean isTaxListEmpty = true;
        
        if (taxList.size() > 0) {
            isTaxListEmpty = false;
        }
        
        model.put("taxList", taxList);
        model.put("isTaxListEmpty", isTaxListEmpty);
        
        return "admin";
    }    
}
