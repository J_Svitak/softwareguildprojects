/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.controller;


import com.mycompany.flooringmastery.dao.TaxDao;
import com.mycompany.flooringmastery.dto.Tax;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/tax")
public class TaxController {
    
    private TaxDao taxDao;
    private List<Tax> taxList = new ArrayList();
    
    @Inject
    public TaxController(TaxDao dao) {
        this.taxDao = dao;
    }
    
    @RequestMapping(value="/create", method=RequestMethod.POST)
    public String create(@ModelAttribute Tax state, Map model) {
        taxDao.create(state);
        
        model.put("isNew", true);
        model.put("isUpdate", false);
        
        return "showtax";
    }
    
    @RequestMapping(value="/delete/{state}", method=RequestMethod.GET)
    public String delete(@PathVariable("state") String state) {
        Tax objState = taxDao.get(state);
        taxDao.delete(objState);
        return "redirect:/";
    }
    
    @RequestMapping(value="/edit/{state}", method=RequestMethod.GET)
    public String edit(@PathVariable("state") String state, Map model) {
        
        Tax objState = taxDao.get(state);
        model.put("tax", objState);
        return "edittax";
    }
    
    @RequestMapping(value="/edit", method=RequestMethod.POST)
    public String editSubmit(@ModelAttribute Tax objState, Map model) {
        taxDao.update(objState);
        
        model.put("isNew", false);
        model.put("isUpdate", true);
        
        return "showtax";
    }
    
    @RequestMapping(value="/show/{state}", method=RequestMethod.GET)
    public String show(@PathVariable("state") String state, Map model) {
        Tax objState = taxDao.get(state);
        model.put("tax", objState);
        model.put("isNew", false);
        model.put("isUpdate", false);
        
        return "showtax";
    }
    
    @RequestMapping(value="/filter", method=RequestMethod.POST)
    public String filterAddressList(@RequestParam("stateFilterValue") String filterValue, Map model) {
        
        taxList = new ArrayList();
         
        Tax ObjTax = taxDao.get(filterValue);
                
        if (ObjTax != null) {
            taxList.add(ObjTax);
        }        
        
        boolean isTaxListEmpty = true;
        if (taxList.size() > 0) {
            isTaxListEmpty = false;
        }
        
        model.put("taxList", taxList);
        model.put("isProductListEmpty", isTaxListEmpty);
        
        return "admin";
    }    
}
