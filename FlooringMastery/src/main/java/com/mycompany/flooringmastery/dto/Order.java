/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dto;

import java.util.Date;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Order {
    
    private int orderNumber;
    private String orderDate;
    private String customerName;
    private String state;
    private double taxRate;
    private String productType;
    private double area;
    private double costPerSqFt;
    private double laborCostPerSqFt;
    private double materialCost;
    private double totalLaborCost;
    private double tax;
    private double orderTotal;
    
    public Order() {
        
    }

    public int getOrderNumber() {
        return orderNumber;
    }
    
    @NotEmpty(message="Order Date is Required")
    public String getOrderDate() {
        return orderDate;
    }
    
    @NotEmpty(message="Customer Name is Required")
    public String getCustomerName() {
        return customerName;
    }

    @NotEmpty(message="State is Required")
    public String getState() {
        return state;
    }

    public double getTaxRate() {
        return taxRate;
    }

    @NotEmpty(message="Product Type is Required")
    public String getProductType() {
        return productType;
    }

    public double getArea() {
        return area;
    }

    public double getCostPerSqFt() {
        return costPerSqFt;
    }

    public double getLaborCostPerSqFt() {
        return laborCostPerSqFt;
    }

    public double getMaterialCost() {
        return materialCost;
    }

    public double getTotalLaborCost() {
        return totalLaborCost;
    }

    public double getTax() {
        return tax;
    }

    public double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
    
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setCostPerSqFt(double costPerSqFt) {
        this.costPerSqFt = costPerSqFt;
    }

    public void setLaborCostPerSqFt(double laborCostPerSqFt) {
        this.laborCostPerSqFt = laborCostPerSqFt;
    }

    public void setMaterialCost(double materialCost) {
        this.materialCost = materialCost;
    }

    public void setTotalLaborCost(double totalLaborCost) {
        this.totalLaborCost = totalLaborCost;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public void setOrderTotal(double orderTotal) {
        this.orderTotal = orderTotal;
    }    
}
