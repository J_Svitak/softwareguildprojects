/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flooringmastery.dto;

import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Product {
    
    @NotEmpty(message = "Product Type is Required.")
    private String productType;
    
    @Min(0)
    private double costPerSqFt;
    
    @Min(0)
    private double laborCostperSqFt;
    
    public Product() {
        
    }

    public String getProductType() {
        return productType;
    }

    public double getCostPerSqFt() {
        return costPerSqFt;
    }

    public double getLaborCostperSqFt() {
        return laborCostperSqFt;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setCostPerSqFt(double costPerSqFt) {
        this.costPerSqFt = costPerSqFt;
    }

    public void setLaborCostperSqFt(double laborCostperSqFt) {
        this.laborCostperSqFt = laborCostperSqFt;
    }    
}
