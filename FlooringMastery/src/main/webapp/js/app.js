/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    
    $("#submitCreate").on("click", function(e) {
       
        e.preventDefault();
        
        var orderData = JSON.stringify({           
           orderDate: $("#orderDate").val(),
           customerName: $("#orderCustomerName").val(),
           state: $("#orderState").val(),
           productType: $("#orderProductType").val(),
           area: $("#orderArea").val()
        });
        
        console.log(orderData);
        
        $.ajax({
           url: contextRoot + "/order/",
           type: "POST",
           data: orderData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
                var orderRow = buildOrderRow(data);
                $("#orderTable").append($(orderRow));

                $("orderNumber").val("");
                $("#orderDate").val("");
                $("#orderCustomerName").val("");
                $("#orderState").val("");
                $("#orderProductType").val("");
                $("#orderArea").val("");
                
                setValuesForShowModal(data);                                
                
                $("#showModalSuccessHead").css("display", "");                
                $("#showOrderModal").modal();
          },
          error: function(data, status) {
              
              $("#warnOrderDate").empty();
              $("#orderDate").addClass("");
              $("#warnCustomerName").empty();
              $("#orderCustomerName").addClass("");
              $("#warnState").empty();
              $("#orderState").addClass("");
              $("#warnProductType").empty();
              $("#orderProductType").addClass("");
              
              var errors = data.responseJSON.errors;
              
              $.each(errors, function(index, error) {
                  
                  switch (error.fieldName) {
                      case "orderDate":
                        $("#warnOrderDate").append(error.message);
                        $("#orderDate").addClass("inputError");
                        break;
                    case "customerName":
                        $("#warnCustomerName").append(error.message);
                        $("#orderCustomerName").addClass("inputError");
                        break;
                    case "state":
                        $("#warnState").append(error.message);
                        $("#orderState").addClass("inputError");
                        break;
                    case "productType":
                        $("#warnProductType").append(error.message);
                        $("#orderProductType").addClass("inputError");
                        break;
                    default:
                        break;
                  }                  
              });
          }
        });
    });
    
    $("#submitEdit").on("click", function(e) {
       
        e.preventDefault();
        
        var orderData = JSON.stringify({
           orderNumber: $("#editOrderNumber").val(),
           orderDate: $("#editOrderDate").val(),
           customerName: $("#editCustomerName").val(),
           state: $("#editState").val(),
           productType: $("#editProductType").val(),
           area: $("#editArea").val()          
        });
        
        $.ajax({
           url: contextRoot + "/order/",
           type: "PUT",
           data: orderData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
              $("#editOrderModal").modal("hide");
              
              var orderRow = buildOrderRow(data);
              $("#orderRow-" + data.orderNumber).replaceWith(orderRow);
          },
          error: function(data, status) {
              $("#warnOrderDateEdit").empty();              
              $("#warnCustomerNameEdit").empty();                            
              $("#warnStateEdit").addClass("");              
              $("#warnProductTypeEdit").addClass("");
              
              var errors = data.responseJSON.errors;
              
              $.each(errors, function(index, error) {
                  
                  switch (error.fieldName) {
                      case "orderDate":
                        $("#warnOrderDateEdit").append(error.message);
                        $("#editOrderDate").addClass("inputError");
                        break;
                    case "customerName":
                        $("#warnCustomerNameEdit").append(error.message);
                        $("#editCustomerName").addClass("inputError");
                        break;
                    case "state":
                        $("#warnStateEdit").append(error.message);
                        $("#editState").addClass("inputError");
                        break;
                    case "productType":
                        $("#warnProductTypeEdit").append(error.message);
                        $("#editProductType").addClass("inputError");
                        break;
                    default:
                        break;
                  }                  
              });
          }
        });
    });
    
    $("#submitDelete").on("click", function(e) {
        
        var orderNum = $("#deleteOrderNumber").text();
        console.log(orderNum);
        
        $.ajax({
          type: "DELETE",
          url: contextRoot + "/order/" + orderNum,
          success: function(data, status) {
              $("#orderRow-" + orderNum).remove();
              
              $("#deleteOrderNumber").text("");
              $("#deleteOrderDate").text("");
              $("#deleteCustomerName").text("");
              
              $("#deleteOrderModal").modal("hide");
          },
          error: function(data, status) {
              alert("There was an issue deleting contact " + orderNum + "! \n"
                + status);
          }
      });
    });
    
    $('#showOrderModal').on('show.bs.modal', function(e) {
       
       var link = $(e.relatedTarget);
       var orderNum = link.data("ordernum");
       
       $.ajax({
            url: contextRoot + "/order/" + orderNum,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                $("#showModalSuccessHead").css("display", "none");
                setValuesForShowModal(data);
            },
            error: function(data, status) {
                
            }            
       });
   });
   
    $('#editOrderModal').on('show.bs.modal', function(e) {
       
       var link = $(e.relatedTarget);
       var orderNum = link.data("ordernum");
       
       $.ajax({
            url: contextRoot + "/order/" + orderNum,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {

                $("#editOrderNumber").val(data.orderNumber);
                $("#editOrderDate").val(data.orderDate);
                $("#editCustomerName").val(data.customerName);
                $("#editState").val(data.state);
                $("#editProductType").val(data.productType);
                $("#editArea").val(data.area);
            },
            error: function(data, status) {
              alert(error);
            }            
       });
   });
   
    $(document).on("click", ".delete-link", function(e) {
       
      e.preventDefault();      
      var orderNum = $(e.target).data("ordernum");
      console.log("id: " + orderNum);      
      
      $.ajax({
            url: contextRoot + "/order/" + orderNum,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                
                $("#deleteOrderNumber").text(data.orderNumber);
                $("#deleteOrderDate").text(data.orderDate);
                $("#deleteCustomerName").text(data.customerName);
                
                $("#deleteOrderModal").modal();
            },
            error: function(data, status) {
                
            }            
       });      
   });
    
    $("#submitOrderFilter").on("click", function(e) {
        e.preventDefault();
        
        filterValue = $("#filterValue").val();
        
        var orderData = JSON.stringify({
           orderNumber: $("#orderNumber").val(),
           orderDate: $("#orderDate").val(),
           customerName: $("#orderCustomerName").val(),
           state: $("#orderState").val(),
           productType: $("#orderProductType").val(),
           area: $("#orderArea").val()
        });
        
        console.log(orderData);
        
        $.ajax({
           url: contextRoot + "/order/filter/" + filterValue,
           type: "POST",
           data: orderData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {              
              
              $("#orderTable tbody").empty();
              
              for (var i = 0; i < data.length; i++) {
                  console.log(data[i]);                  
                  
                  var orderRow = buildOrderRow(data[i]);
                  $("#orderTable").append($(orderRow));
              }                
          },
          error: function(data, status) {
              alert("error");
          }
        });
    });
    
    $("#submitClearFilter").on("click", function(e) {
        e.preventDefault();
        
        var orderData = JSON.stringify({
           orderNumber: $("#orderNumber").val(),
           orderDate: $("#orderDate").val(),
           customerName: $("#orderCustomerName").val(),
           state: $("#orderState").val(),
           productType: $("#orderProductType").val(),
           area: $("#orderArea").val()
        });
        
        console.log(orderData);
        
        $.ajax({
           url: contextRoot + "/order/orderlist/",
           type: "POST",
           data: orderData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {              
              
              $("#orderTable tbody").empty();
              
              for (var i = 0; i < data.length; i++) {
                  console.log(data[i]);                  
                  
                  var orderRow = buildOrderRow(data[i]);
                  $("#orderTable").append($(orderRow));
              }                
          },
          error: function(data, status) {
              alert("error");
          }
        });
    });
    
    function buildOrderRow(data) {
        return "<tr id='orderRow-" + data.orderNumber + "'>  \n\
                <td><a data-ordernum='" + data.orderNumber +"' data-toggle='modal' data-target='#showOrderModal'>" + data.orderNumber + "</a></td>  \n\
                <td> " + data.orderDate + "</td>    \n\
                <td> " + data.customerName + "</td>    \n\
                <td> <a data-ordernum='" + data.orderNumber +"' data-toggle='modal' data-target='#editOrderModal'><i class='glyphicon glyphicon-pencil'></i></a>  </td>   \n\
                <td> <a data-ordernum='" + data.orderNumber +"' class='delete-link'><i class='glyphicon glyphicon-remove'></i></a>  </td>   \n\
                </tr>  ";
        }
        
    function setValuesForShowModal(data) {
        $("#showOrderNumber").text(data.orderNumber);
        $("#showOrderDate").text(data.orderDate);
        $("#showCustomerName").text(data.customerName);
        $("#showState").text(data.state);
        $("#showTaxRate").text(data.taxRate);
        $("#showProductType").text(data.productType);
        $("#showArea").text(data.area);        
    }
});


