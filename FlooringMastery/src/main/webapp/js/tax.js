/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    
    $("#submitCreateProduct").on("click", function(e) {
       
        e.preventDefault();
        
        var productData = JSON.stringify({           
           productType: $("#productType").val(),
           costPerSqFt: $("#costPerSqFt").val(),
           laborCostPerSqFt: $("#laborCostperSqFt").val()           
        });
        
        console.log(productData);
        
        $.ajax({
           url: contextRoot + "/product/",
           type: "POST",
           data: productData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
                var productRow = buildProductRow(data);
                $("#orderTable").append($(productRow));
                
                $("#productType").val("");
                $("#costPerSqFt").val("");
                $("#laborCostperSqFt").val("");
                
                setValuesForShowModal(data);                                
                
                $("#showModalSuccessHead").css("display", "");                
                $("#showOrderModal").modal();
          },
          error: function(data, status) {
              
              $("#warnProductType").empty();
              $("#warnMaterialCost").addClass("");
              $("#warnLaborCost").empty();              
              
              var errors = data.responseJSON.errors;
              
              $.each(errors, function(index, error) {
                  
                  switch (error.fieldName) {
                      case "productType":
                        $("#warnProductType").append(error.message);
                        $("#productType").addClass("inputError");
                        break;
                    case "customerName":
                        $("#warnMaterialCost").append(error.message);
                        $("#costPerSqFt").addClass("inputError");
                        break;
                    case "state":
                        $("#warnLaborCost").append(error.message);
                        $("#laborCostperSqFt").addClass("inputError");
                        break;                    
                    default:
                        break;
                  }                  
              });
          }
        });
    });
    
    $("#submitEdit").on("click", function(e) {
       
        e.preventDefault();
        
        var productData = JSON.stringify({
           productType: $("#editProductType").val(),
           costPerSqFt: $("#editMaterialCost").val(),
           laborCostperSqFt: $("#editLaborCost").val(),           
        });
        
        $.ajax({
           url: contextRoot + "/product/",
           type: "PUT",
           data: productData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
              $("#editProductModalModal").modal("hide");
              
              var productRow = buildProductRow(data);
              $("#productRow-" + data.productType).replaceWith(productRow);
          },
          error: function(data, status) {
              $("#warnProductTypeEdit").empty();
              $("#warnMaterialCostEdit").empty();                            
              $("#warnLaborCostEdit").empty();                            
              
              
              var errors = data.responseJSON.errors;
              
              $.each(errors, function(index, error) {
                  
                  switch (error.fieldName) {
                      case "productType":
                        $("#warnProductTypeEdit").append(error.message);
                        $("#editProductType").addClass("inputError");
                        break;
                    case "costPerSqFt":
                        $("#warnMaterialCostEdit").append(error.message);
                        $("#editMaterialCost").addClass("inputError");
                        break;
                    case "laborCostperSqFt":
                        $("#warnLaborCostEdit").append(error.message);
                        $("#editLaborCost").addClass("inputError");
                        break;                    
                    default:
                        break;
                  }                  
              });
          }
        });
    });
    
    $("#submitDelete").on("click", function(e) {
        
        var productType = $("#deleteProductType").text();
        console.log(productType);
        
        $.ajax({
          type: "DELETE",
          url: contextRoot + "/product/" + productType,
          success: function(data, status) {
              $("#productRow-" + productType).remove();
              
              $("#deleteProductType").text("");
              $("#deleteMaterialCost").text("");
              $("#deleteLaborCost").text("");
              
              $("#deleteProductModal").modal("hide");
          },
          error: function(data, status) {
              alert("There was an issue deleting contact " + productType + "! \n"
                + status);
          }
      });
    });
    
    $('#showProductModal').on('show.bs.modal', function(e) {
       
       var link = $(e.relatedTarget);
       var productType = link.data("producttype");
       
       $.ajax({
            url: contextRoot + "/product/" + productType,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                $("#showModalSuccessHead").css("display", "none");
                setValuesForShowModal(data);
            },
            error: function(data, status) {
                
            }            
       });
   });
   
    $('#editProductModal').on('show.bs.modal', function(e) {
       
       var link = $(e.relatedTarget);
       var productType = link.data("producttype");
       
       $.ajax({
            url: contextRoot + "/product/" + productType,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {

                $("#editProductType").val(data.productType);
                $("#editMaterialCost").val(data.costPerSqFt);
                $("#editLaborCost").val(data.laborCostperSqFt);                
            },
            error: function(data, status) {
              alert(error);
            }            
       });
   });
   
    $(document).on("click", ".delete-link", function(e) {
       
      e.preventDefault();      
      var productType = $(e.target).data("producttype");
      console.log("id: " + productType);      
      
      $.ajax({
            url: contextRoot + "/product/" + productType,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                
                $("#deleteOrderNumber").text(data.orderNumber);
                $("#deleteOrderDate").text(data.orderDate);
                $("#deleteCustomerName").text(data.customerName);
                
                $("#deleteOrderModal").modal();
            },
            error: function(data, status) {
                
            }            
       });      
   });
    
    $("#submitOrderFilter").on("click", function(e) {
        e.preventDefault();
        
        filterValue = $("#filterValue").val();
        
        var orderData = JSON.stringify({
           orderNumber: $("#orderNumber").val(),
           orderDate: $("#orderDate").val(),
           customerName: $("#orderCustomerName").val(),
           state: $("#orderState").val(),
           productType: $("#orderProductType").val(),
           area: $("#orderArea").val()
        });
        
        console.log(orderData);
        
        $.ajax({
           url: contextRoot + "/order/filter/" + filterValue,
           type: "POST",
           data: orderData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {              
              
              $("#orderTable tbody").empty();
              
              for (var i = 0; i < data.length; i++) {
                  console.log(data[i]);                  
                  
                  var orderRow = buildOrderRow(data[i]);
                  $("#orderTable").append($(orderRow));
              }                
          },
          error: function(data, status) {
              alert("error");
          }
        });
    });
    
    $("#submitClearFilter").on("click", function(e) {
        e.preventDefault();
        
        var orderData = JSON.stringify({
           orderNumber: $("#orderNumber").val(),
           orderDate: $("#orderDate").val(),
           customerName: $("#orderCustomerName").val(),
           state: $("#orderState").val(),
           productType: $("#orderProductType").val(),
           area: $("#orderArea").val()
        });
        
        console.log(orderData);
        
        $.ajax({
           url: contextRoot + "/order/orderlist/",
           type: "POST",
           data: orderData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {              
              
              $("#orderTable tbody").empty();
              
              for (var i = 0; i < data.length; i++) {
                  console.log(data[i]);                  
                  
                  var orderRow = buildOrderRow(data[i]);
                  $("#orderTable").append($(orderRow));
              }                
          },
          error: function(data, status) {
              alert("error");
          }
        });
    });
    
    function buildProductRow(data) {
        return "<tr id='productRow-" + data.productType + "'>  \n\
                <td><a data-producttype='" + data.productType +"' data-toggle='modal' data-target='#showProductModal'>" + data.productType + "</a></td>  \n\
                <td> " + data.costPerSqFt + "</td>    \n\
                <td> " + data.laborCostPerSqFt + "</td>    \n\
                <td> <a data-producttype='" + data.productType +"' data-toggle='modal' data-target='#editProductModal'><i class='glyphicon glyphicon-pencil'></i></a>  </td>   \n\
                <td> <a data-producttype='" + data.productType +"' class='delete-link'>Delete</a>  </td>   \n\
                </tr>  ";
        }
        
    function setValuesForShowModal(data) {
        $("#showProductType").text(data.productType);
        $("#showMaterialCost").text(data.costPerSqFt);
        $("#showLaborCost").text(data.laborCostperSqFt);                
    }
});


