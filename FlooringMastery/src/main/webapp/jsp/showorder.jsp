<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    <style>
            .header {
                width: 99%;
                text-align: center;
                color: #FDBB30;
                background-color: #002D62;
                border: 1px solid #FDBB30;
                font-weight: bold;               
            }
            .pageContent {
                width: 100%;                    
                background-color: whitesmoke;
                border: 1px solid #002D62;                
                padding-bottom: 10px;
            }
            .footer {
                width: 99%;
                text-align: center;
                color: #860038;
                background-color: #FDBB30;
                border: 1px solid #002D62;
                margin-top: 5px;
                float: left;
            }
        </style>
    </head>
    <body>
        <div class="container col-md-12" style="background-color: whitesmoke; border: 1px solid #002D62; background-color: #860038; padding: 3px 0 3px 5px;">
            <h1 class="header">Flooring Mastery</h1>
            <div class="navbar" style="background-color: #FDBB30">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                 <li role="presentation" class="active"><a href="">Order Summary</a></li>
                </ul>    
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">    
                <div class="row">                
                    <div class="col-md-6" style="background-color: whitesmoke; padding-bottom: 10px"">
                        <h3 class="header">Order Summary</h3>
                        <div class="pageContent">
                            <div class="form-group" style="width: 100%; color: green; font-weight: bold; text-align: center">
                                <c:if test="${isNew}" >
                                    Order ${order.orderNumber} was successfully created!
                                </c:if>
                                <c:if test="${isUpdate}" >
                                    Order ${order.orderNumber} was successfully updated!
                                </c:if>
                            </div>
                            <div class="form-group">
                                <label for="divOrderNumber" class="col-md-4 control-label">Order #:</label>
                                <div id="divOrderNumber">
                                    ${order.orderNumber}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divOrderDate" class="col-md-4 control-label">Order Date:</label>
                                <div id="divOrderDate">
                                    ${order.orderDate}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divCustomerName" class="col-md-4 control-label">Customer Name:</label>
                                <div id="divCustomerName">
                                    ${order.customerName}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divState" class="col-md-4 control-label">State:</label>
                                <div id="divState">
                                    ${order.state}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divProductType" class="col-md-4 control-label">Product Type:</label>
                                <div id="divProductType">
                                    ${order.productType}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divArea" class="col-md-4 control-label">Area:</label>
                                <div id="divArea">
                                    ${order.area}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divCostPerSqFt" class="col-md-4 control-label">Cost Per Sq Ft:</label>
                                <div id="divCostPerSqFt">
                                    ${order.costPerSqFt}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divMaterialCost" class="col-md-4 control-label">Material Cost:</label>
                                <div id="divMaterialCost">
                                    ${order.materialCost}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divLaborCostPerSqFt" class="col-md-4 control-label">Labor Per Sq Ft:</label>
                                <div id="divLaborCostPerSqFt">
                                    ${order.laborCostPerSqFt}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divLaborCost" class="col-md-4 control-label">Labor Cost:</label>
                                <div id="divLaborCost">
                                    ${order.totalLaborCost}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divTaxAmount" class="col-md-4 control-label">Tax Amount:</label>
                                <div id="divTaxAmount">
                                    ${order.tax}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divOrderTotal" class="col-md-4 control-label">Order Total:</label>
                                <div id="divOrderTotal">
                                    ${order.orderTotal}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                            
            </div>
            <div class="col-md-1"></div>
            <div class="footer">
                Created by James Svitak
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

