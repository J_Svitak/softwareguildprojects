<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Mastery Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <!--<link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">-->
        <link href="${pageContext.request.contextPath}/css/flooring.css" rel="stylesheet">
        
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body style="color: #002D62">
        <div class="container col-md-12 cont-background">
            <h1 class="header">Flooring Mastery Administration</h1>            
            <div class="navbar navbar-background">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/admin">Administration</a></li>
                </ul>    
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row row-background">                    
                    <div class="col-md-6" style="border: 1px solid #002D62; height: 415px; padding-bottom: 3px">
                        <h3 class="header" style="width:100%">Current Products</h3>
                        <div class="pageContent" style="height: 300px; overflow-y: scroll">
                            <table class="table table-bordered" id="productTable">
                                <thead>
                                    <tr>
                                        <th>Product Type</th>
                                        <th>Cost Per Sq Ft</th>
                                        <th>Labor Per Sq Ft</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${isProductListEmpty}">
                                        <tr>
                                            <td colspan="3" style="color:red; font-style: italic">No Products were found...</td>
                                        </tr>
                                    </c:if>
                                    <c:forEach items="${productList}" var="product">
                                        <tr id="productRow-${product.productType}">
                                            <td><a data-producttype="${product.productType}" data-toggle="modal" data-target="#showProductModal">${product.productType}</a></td>
                                            <td>${product.costPerSqFt}</td>
                                            <td>${product.laborCostperSqFt}</td>
                                            <td><a data-producttype="${product.productType}" data-toggle="modal" data-target="#editProductModal"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                            <td><a data-producttype="${product.productType}" class="delete-link">Delete</a></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <form method="POST" class="form-horizontal">
                            <div class="pageContent" style="margin-top: 5px; padding: 5px">                                                            
                                <label for="filterValue">Product Type Search</label>
                                <input type="text" name="productFilterValue" id="productFilterValue" style="padding: 0 5px 0 5px" />
                                <input type="submit" value="Filter Products" class="btn btn-primary" />
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6" style="border: 1px solid #002D62; height: 415px">
                        <h3 class="header" style="width:100%">Create New Product</h3>
                        <div class="pageContent" style="height: 300px">
                            <form method="POST" action="product/create" class="form-horizontal" >                                                                                    
                                <div class="form-group">
                                    <label for="productType" class="col-md-4 control-label">Product Type:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="productType" id="productType" class="form-control" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnProductType" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="costPerSqFt" class="col-md-4 control-label">Cost Per Sq Ft:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="costPerSqFt" id="costPerSqFt" class="form-control" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnMaterialCost" class="col-md-8 warning"></div>
                                </div>                                                                                
                                <div class="form-group">
                                    <label for="laborCostperSqFt" class="col-md-4 control-label">Labor Per Sq Ft:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="laborCostperSqFt" id="laborCostperSqFt" class="form-control" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnLaborCost" class="col-md-8 warning"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <input type="submit" value="Create Product" id="submitCreateProduct" class="btn btn-primary"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>                            
                </div>
                <hr/>
                <div class="row row-background">                    
                    <div class="col-md-6" style="border: 1px solid #002D62; padding-bottom: 3px">
                        <h3 class="header" style="width:100%">Current Tax Rates</h3>
                        <div class="pageContent" style="height: 300px; overflow-y: scroll">
                            <table class="table table-bordered">                            
                                <tr>
                                    <th>State</th>
                                    <th>Tax Rate</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                <c:if test="${isTaxListEmpty}">
                                    <tr>
                                        <td colspan="2" style="color:red; font-style: italic">No States were found...</td>
                                    </tr>
                                </c:if>
                                <c:forEach items="${taxList}" var="tax">
                                    <tr>
                                        <td><a href="${pageContext.request.contextPath}/tax/show/${tax.state}">${tax.state}</a></td>
                                        <td>${tax.taxRate}</td>
                                        <td><a href="tax/edit/${tax.state}"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                        <td><a href="tax/delete/${tax.state}">Delete</a></td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                        <form method="POST" action="${pageContext.request.contextPath}/tax/filter" class="form-horizontal">
                            <div class="pageContent" style="margin-top: 5px; padding: 5px">                                                            
                                <label for="stateFilterValue">State/Tax Search</label>
                                <input type="text" name="stateFilterValue" id="filterValue" style="padding: 0 5px 0 5px" />
                                <input type="submit" value="Filter" class="btn btn-primary" />
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form method="POST" action="tax/create" class="form-horizontal" >
                            <div class="pageContent" style="height: 350px">
                                <h3 class="header" style="width:100%">Create New State/Tax</h3>                        
                                <div class="form-group">
                                    <label for="state" class="col-md-4 control-label">State:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="state" id="state" class="form-control" />
                                    </div>
                                </div>                        
                                <div class="form-group">
                                    <label for="taxRate" class="col-md-4 control-label">Tax Rate:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="taxRate" id="taxRate" class="form-control" />
                                    </div>
                                </div>                                                                                                                
                                <div class="form-group" style="text-align: center">
                                    <input type="submit" value="Create Tax" class="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="footer">
                Created by James Svitak
            </div>
        </div>
        <div id="showProductModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Order Summary</h4>
                    </div>
                    <div class="modal-body">
                        <div id="showModalSuccessHead" style="display:none">
                            <h6 style='color: green; font-weight: bold'>Order Successfully Created</h6>
                        </div>
                        <table class="table table-bordered" id="showProductTable">
                            <tr>
                                <th>Product Type:</th>
                                <td id="showProductType"></td>
                            </tr>
                            <tr>
                                <th>Cost Per Sq Ft:</th>
                                <td id="showMaterialCost"></td>
                            </tr>
                            <tr>
                                <th>Labor Cost Per Sq Ft</th>
                                <td id="showLaborCost"></td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="editProductModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Order</h4>
                    </div>
                    <div class="modal-body">                        
                        <table class="table table-bordered" id="editProductTable">
                            <tr>
                                <th>Product Type:</th>
                                <td>
                                    <div>
                                        <input type="text" id="editProductType" />
                                    </div>
                                    <div id="warnProductTypeEdit" class="warning"></div>
                                </td>
                            </tr>
                            <tr>                                
                                <th>Cost Per Sq Ft:</th>
                                <td>
                                    <div>
                                        <input type="text" id="editMaterialCost" class="form-control" />
                                    </div>
                                    <div id="warnMaterialCostEdit" class="warning"></div>
                                </td>                                
                            </tr>
                            <tr>
                                <th>Labor Cost Per Sq Ft:</th>                                
                                <td>
                                    <div>
                                        <input type="text" id="editLaborCost" name="editLaborCost" class="form-control" />
                                    </div>
                                    <div id="warnLaborCostEdit" class="warning"></div>
                                </td>                                                                
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" id="submitEdit">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="deleteProductModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Order Summary</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <h6 style='color: red; font-weight: bold'>Permanently Delete Product?</h6>
                        </div>
                        <table class="table table-bordered" id="deleteProductTable">
                            <tr>
                                <th>Product Type:</th>
                                <td id="deleteProductType"></td>
                            </tr>
                            <tr>
                                <th>Cost Per Sq Ft:</th>
                                <td id="deleteMaterialCost"></td>
                            </tr>
                            <tr>
                                <th>Labor Cost Per Sq Ft:</th>
                                <td id="deleteLaborCost"></td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-primary" id="submitDelete">Delete</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/product.js"></script>
        <script src="${pageContext.request.contextPath}/js/tax.js"></script>
    </body>
</html>

