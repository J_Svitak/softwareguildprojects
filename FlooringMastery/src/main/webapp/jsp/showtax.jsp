<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Mastery Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/flooring.css" rel="stylesheet">
        
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body style="color: #002D62">
        <div class="container col-md-12 cont-background">
            <h1 class="header">Flooring Mastery Administration</h1>            
            <div class="navbar navbar-background">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/admin">Administration</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/tax/show/${tax.state}">Update State/Tax</a></li>
                </ul>    
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">                
                <div class="row row-background">                    
                    <div class="col-md-6" style="background-color: whitesmoke; padding-bottom: 10px">
                        <h3 class="header">State/Tax Summary</h3>
                        <div class="pageContent">
                            <div class="form-group" style="width: 100%; color: green; font-weight: bold; text-align: center">
                                <c:if test="${isNew}" >
                                    ${tax.state} was successfully created!
                                </c:if>
                                <c:if test="${isUpdate}" >
                                    ${tax.state} was successfully updated!
                                </c:if>
                            </div>
                            <div class="form-group">
                                <label for="divState" class="col-md-4 control-label">State:</label>
                                <div id="divState">
                                    ${tax.state}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divTaxRate" class="col-md-4 control-label">Tax Rate:</label>
                                <div id="divTaxRate">
                                    ${tax.taxRate}
                                </div>
                            </div>                            
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="footer">
                Created by James Svitak
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>

