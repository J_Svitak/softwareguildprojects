<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/flooring.css" rel="stylesheet">
        
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    
    </head>
    <body>
        <div class="container col-md-12" style="background-color: whitesmoke; border: 1px solid #002D62; background-color: #860038; padding: 3px 0 3px 5px;">
            <h1 class="header">Flooring Mastery</h1>
            <div class="navbar" style="background-color: #FDBB30">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                 <li role="presentation"><a href="${pageContext.request.contextPath}/admin">Administration</a></li>
                 <li role="presentation" class="active"><a href="">Product Summary</a></li>
                </ul>    
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">    
                <div class="row">                
                    <div class="col-md-6" style="background-color: whitesmoke; padding-bottom: 10px">
                        <h3 class="header">Product Summary</h3>
                        <div class="pageContent">
                            <div class="form-group" style="width: 100%; color: green; font-weight: bold; text-align: center">
                                <c:if test="${isNew}" >
                                    ${product.productType} was successfully created!
                                </c:if>
                                <c:if test="${isUpdate}" >
                                    ${product.productType} was successfully updated!
                                </c:if>
                            </div>
                            <div class="form-group">
                                <label for="divProductType" class="col-md-4 control-label">Product Type:</label>
                                <div id="divProductType">
                                    ${product.productType}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divOrderDate" class="col-md-4 control-label">Cost Per Sq Ft:</label>
                                <div id="divOrderDate">
                                    ${product.costPerSqFt}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="divCustomerName" class="col-md-4 control-label">Labor Per Sq Ft:</label>
                                <div id="divCustomerName">
                                    ${product.laborCostperSqFt}
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>                            
            </div>
            <div class="col-md-1"></div>
            <div class="footer">
                Created by James Svitak
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

