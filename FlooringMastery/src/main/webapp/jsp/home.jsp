<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Mastery Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/flooring.css" rel="stylesheet">
        
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body style="color: #002D62">
        <div class="container col-md-12 cont-background">
            <h1 class="header">Flooring Mastery</h1>            
            <div class="navbar navbar-background">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/admin">Administration</a></li>
                </ul>    
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row row-background">                
                    <div class="col-md-7" style="border: 1px solid #002D62; padding-bottom: 3px; height: 500px">                                                                    
                        <h3 class="header">Latest Orders</h3>
                        <div class="pageContent" style="height: 350px; overflow-y: scroll">
                            <table class="table table-bordered" id="orderTable">
                                <thead>
                                    <tr>
                                        <th>Order Number</th>
                                        <th>Order Date</th>
                                        <th>Customer Name</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${isOrderListEmpty}">
                                        <tr>
                                            <td colspan="5" style="color:red; font-style: italic">No Orders were found...</td>
                                        </tr>
                                    </c:if>
                                    <c:forEach items="${orderList}" var="order">
                                        <tr id="orderRow-${order.orderNumber}">
                                            <td><a data-ordernum="${order.orderNumber}" data-toggle="modal" data-target="#showOrderModal">${order.orderNumber}</a></td>
                                            <td>${order.orderDate}</td>
                                            <td>${order.customerName}</td>
                                            <td><a data-ordernum="${order.orderNumber}" data-toggle="modal" data-target="#editOrderModal"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                            <td><a data-ordernum="${order.orderNumber}" class="delete-link">Delete</a></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <form method="POST" action="order/filter" class="form-horizontal">
                            <div class="pageContent" style="margin-top: 5px; padding: 5px">                                
                                <label for="filterValue">Search Value</label>
                                <input type="text" name="filterValue" id="filterValue" style="padding: 0 5px 0 5px" />
                                <input type="submit" id="submitOrderFilter" value="Filter" class="btn btn-primary" />
                                <input type="submit" id="submitClearFilter" value="Display All Orders" class="btn btn-primary" />
                            </div>
                        </form>
                    </div>
                    <div class="col-md-5" style="border: 1px solid #002D62; height: 500px">
                        <h3 class="header" style="width:100%">Create New Order</h3>
                        <div class="pageContent" style="height: 350px; padding: 10px 5px 0 0">
                            <form method="POST" class="form-horizontal" >                            
                                <div class="form-group">
                                    <label for="orderDate" class="col-md-4 control-label">Order Date:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="orderDate" id="orderDate" class="form-control" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnOrderDate" class="col-md-8 warning"></div>
                                </div>                                
                                <div class="form-group">
                                    <label for="orderCustomerName" class="col-md-4 control-label">Customer Name:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="orderCustomerName" id="orderCustomerName" class="form-control" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnCustomerName" class="col-md-8 warning"></div>
                                </div>
                                <div class="form-group">
                                    <label for="orderState" class="col-md-4 control-label">State:</label>
                                    <div class="col-md-8">
                                        <select name="orderState" id="orderState" class="form-control">
                                            <option value=""></option>
                                            <c:forEach items="${taxList}" var="state">
                                                <option value="${state.state}">${state.state}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnState" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="productType" class="col-md-4 control-label">Product Type:</label>
                                    <div class="col-md-8">                                        
                                        <select name="orderProductType" id="orderProductType" class="form-control">
                                            <option value=""></option>
                                            <c:forEach items="${productList}" var="product">
                                                <option value="${product.productType}">${product.productType}</option>
                                            </c:forEach>
                                        </select>    
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnProductType" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="orderArea" class="col-md-4 control-label">Total Area:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="orderArea" id="orderArea" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group" style="text-align: center">
                                    <input type="submit" id="submitCreate" value="Create Order" class="btn btn-primary"/>
                                </div>                            
                            </form>
                        </div>
                    </div>                            
                </div>
                <hr/>
                <div class="row row-background">
                    <div class="col-md-6" style="border: 1px solid #002D62; padding-bottom: 5px">
                        <h3 class="header" style="width:100%">Current Products</h3>
                        <div class="pageContent" style="height: 350px; overflow-y: scroll">
                            <table class="table table-bordered">                            
                                <tr>
                                    <th>Product Type</th>
                                    <th>Cost Per Sq Ft</th>
                                    <th>Labor Per Sq Ft</th>                            
                                </tr>
                                <c:if test="${isProductListEmpty}">
                                    <tr>
                                        <td colspan="3" style="color:red; font-style: italic">No Products were found...</td>
                                    </tr>
                                </c:if>
                                <c:forEach items="${productList}" var="product">
                                    <tr>
                                        <td>${product.productType}</td>
                                        <td>${product.costPerSqFt}</td>
                                        <td>${product.laborCostperSqFt}</td>                                
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                   <div class="col-md-6" style="border: 1px solid #002D62; padding-bottom: 5px">
                        <h3 class="header" style="width:100%">State Tax Rates</h3>
                        <div class="pageContent" style="height: 350px; overflow-y: scroll">                        
                        <table class="table table-bordered">                            
                            <tr>
                                <th>State</th>
                                <th>Tax Rate</th>                            
                            </tr>
                            <c:if test="${isTaxListEmpty}">
                                <tr>
                                    <td colspan="2" style="color:red; font-style: italic">No States were found...</td>
                                </tr>
                            </c:if>
                            <c:forEach items="${taxList}" var="tax">
                                <tr>
                                    <td>${tax.state}</td>
                                    <td>${tax.taxRate}</td>                                
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
            <div class="footer">
                Created by James Svitak
            </div>
        </div>
        
        <div id="showOrderModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Order Summary</h4>
                    </div>
                    <div class="modal-body">
                        <div id="showModalSuccessHead" style="display:none">
                            <h6 style='color: green; font-weight: bold'>Order Successfully Created</h6>
                        </div>
                        <table class="table table-bordered" id="showOrderTable">
                            <tr>
                                <th>Order #:</th>
                                <td id="showOrderNumber"></td>
                            </tr>
                            <tr>
                                <th>Order Date:</th>
                                <td id="showOrderDate"></td>
                            </tr>
                            <tr>
                                <th>Customer Name</th>
                                <td id="showCustomerName"></td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td id="showState"></td>
                            </tr>
                            <tr>
                                <th>Tax Rate:</th>
                                <td id="showTaxRate"></td>
                            </tr>
                            <tr>
                                <th>Product Type</th>
                                <td id="showProductType"></td>
                            </tr>
                            <tr>
                                <th>Area:</th>
                                <td id="showArea"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="editOrderModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Order</h4>
                    </div>
                    <div class="modal-body">                        
                        <table class="table table-bordered" id="editOrderTable">
                            <tr>
                                <th>Order #:</th>
                                <td>
                                    <input type="text" id="editOrderNumber" readOnly="readOnly" class="input-readOnly" />
                                </td>
                            </tr>
                            <tr>                                
                                <th>Order Date:</th>
                                <td>
                                    <div>
                                        <input type="text" id="editOrderDate" class="form-control" />
                                    </div>
                                    <div id="warnOrderDateEdit" class="warning"></div>
                                </td>                                
                            </tr>
                            <tr>
                                <th>Customer Name:</th>                                
                                <td>
                                    <div>
                                        <input type="text" id="editCustomerName" name="editCustomerName" class="form-control" />
                                    </div>
                                    <div id="warnCustomerNameEdit" class="warning"></div>
                                </td>                                                                
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td>
                                    <div>
                                        <select name="editState" id="editState" class="form-control" >
                                            <option value=""></option>
                                            <c:forEach items="${taxList}" var="state">
                                                <option value="${state.state}">${state.state}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div id="warnStateEdit" class="warning"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Product Type:</th>
                                <td>
                                    <div>
                                        <select id="editProductType" class="form-control">
                                            <option value=""></option>
                                            <c:forEach items="${productList}" var="product">
                                                <option value="${product.productType}">${product.productType}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div id="warnProductTypeEdit" class="warning"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Area:</th>
                                <td>
                                    <div>
                                        <input type="text" id="editArea" />
                                    </div>
                                    <div id="warnAreaEdit" class="warning"></div>
                                </td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" id="submitEdit">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="deleteOrderModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Order Summary</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <h6 style='color: red; font-weight: bold'>Permanently Delete Order?</h6>
                        </div>
                        <table class="table table-bordered" id="deleteOrderTable">
                            <tr>
                                <th>Order #:</th>
                                <td id="deleteOrderNumber"></td>
                            </tr>
                            <tr>
                                <th>Order Date:</th>
                                <td id="deleteOrderDate"></td>
                            </tr>
                            <tr>
                                <th>Customer Name</th>
                                <td id="deleteCustomerName"></td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-primary" id="submitDelete">Delete</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>
    </body>
</html>

