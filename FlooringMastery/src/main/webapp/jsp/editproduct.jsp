<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Mastery Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/flooring.css" rel="stylesheet">
        
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body style="color: #002D62">
        <div class="container col-md-12 cont-background">
            <h1 class="header">Flooring Mastery Administration</h1>            
            <div class="navbar navbar-background">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/admin">Administration</a></li>
                    <li role="presentation" class="active"><a href="">Update Order</a></li>
                </ul>    
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row row-background">                    
                    <div class="col-md-6">
                        <form method="POST" action="./" class="form-horizontal" >
                            <div class="pageContent" style="height: 350px">
                                <h3 class="header" style="width:100%">Update Product</h3>                        
                                <div class="form-group">
                                    <label for="productType" class="col-md-4 control-label">Product Type:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="productType" id="productType" class="form-control" value="${product.productType}" />
                                    </div>
                                </div>                        
                                <div class="form-group">
                                    <label for="costPerSqFt" class="col-md-4 control-label">Cost Per Sq Ft:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="costPerSqFt" id="costPerSqFt" class="form-control" value="${product.costPerSqFt}" />
                                    </div>
                                </div>                                                                                
                                <div class="form-group">
                                    <label for="laborCostperSqFt" class="col-md-4 control-label">Labor Per Sq Ft:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="laborCostperSqFt" id="laborCostperSqFt" class="form-control" value="${product.laborCostperSqFt}" />
                                    </div>
                                </div>
                                <div class="form-group" style="text-align: center">
                                    <input type="submit" value="Update Product" class="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    </div>                            
                </div>               
            </div>
            <div class="footer">
                Created by James Svitak
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>

