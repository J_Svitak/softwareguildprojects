/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    
    $("#submitCreate").on("click", function(e) {
       
        e.preventDefault();
        
        var dvdData = JSON.stringify({           
           title: $("#dvdTitle").val(),
           releaseDate: $("#dvdReleaseDate").val(),
           rated: $("#dvdRated").val(),
           director: $("#dvdDirector").val(),
           studio: $("#dvdStudio").val()
        });
        
        $.ajax({
           url: contextRoot + "/dvd/",
           type: "POST",
           data: dvdData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
                var dvdRow = buildDvdRow(data);
                $("#dvdTable").append($(dvdRow));

                $("#dvdTitle").val("");
                $("#dvdReleaseDate").val("");
                $("#dvdRated").val("");
                $("#dvdDirector").val("");
                $("#dvdStudio").val("");
                
                setValuesForShowModal(data);                                
                
                $("#showModalSuccessHead").css("display", "");                
                $("#showDvdModal").modal();
          },
          error: function(data, status) {
              
              $("#warnDvdTitle").empty();
              $("#warnReleaseDate").empty();
              $("#warnRated").empty();
              $("#warnDirector").empty();
              $("#warnStudio").empty();
              
              var errors = data.responseJSON.errors;
              
              $.each(errors, function(index, error) {
                  
                  console.log(error.fieldName + ": " + error.message);
                
                switch(error.fieldName) {
                    case "title":
                        $("#warnDvdTitle").append(error.message);
                        break;
                    case "releaseDate":
                        $("#warnReleaseDate").append(error.message);
                        break;
                    case "rated":
                        $("#warnRated").append(error.message);
                        break;
                    case "director":
                        $("#warnDirector").append(error.message);
                        break;
                    case "studio":
                        $("#warnStudio").append(error.message);
                        break;
                    default:
                        break;
                }
              });
              
              return;
          }
        });
    });
    
    $("#submitEdit").on("click", function(e) {
       
        e.preventDefault();
        
        var dvdData = JSON.stringify({
           id: $("#editId").val(),
           title: $("#editTitle").val(),
           releaseDate: $("#editReleaseDate").val(),
           rated: $("#editRated").val(),
           director: $("#editDirector").val(),
           studio: $("#editStudio").val()          
        });
        
        $.ajax({
           url: contextRoot + "/dvd/",
           type: "PUT",
           data: dvdData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
              $("#editDvdModal").modal("hide");
              
              var dvdRow = buildDvdRow(data);
              $("#dvdRow-" + data.id).replaceWith(dvdRow);
          },
          error: function(data, status) {
              alert("error");
          }
        });
    });
    
    $("#submitDvdFilter").on("click", function(e) {
        e.preventDefault();
        
        filterValue = $("#filterValue").val();
        
        if (filterValue == "") {
            return;
        }        
        
        $.ajax({
           url: contextRoot + "/dvd/filter/" + filterValue,
           type: "POST",           
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");              
          },
          success: function(data, status) {              
              
              console.log(data);
              
              $("#dvdTable tbody").empty();
              
              for (var i = 0; i < data.length; i++) {
                  console.log(data[i]);                  
                  
                  if (data[i] != null) {
                    var dvdRow = buildDvdRow(data[i]);
                    $("#dvdTable").append($(dvdRow));
                }
              }                
          },
          error: function(data, status) {
              alert("error");
          }
        });
    });
    
    $('#showDvdModal').on('show.bs.modal', function(e) {
       
       var link = $(e.relatedTarget);
       var dvdId = link.data("dvdid");
       
       $.ajax({
            url: contextRoot + "/dvd/" + dvdId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                $("#showModalSuccessHead").css("display", "none");
                setValuesForShowModal(data);
                
            },
            error: function(data, status) {
                //alert("error loading dvd info");                
            }            
       });
       
       $("#noteTableBody").empty();
       
       //Get the Notes for the Dvd
       $.ajax({
           url: contextRoot + "/note/dvdnotes/" + dvdId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                
                for (var i = 0; i < data.length; i++) {
                    var noteRow = buildNoteRow(data[i]);
                    $("#noteTableBody").append($(noteRow));
                }
            },
            error: function(data, status) {
                //alert("error loading notes");
                return;
            }
       });
   });
   
    $('#editDvdModal').on('show.bs.modal', function(e) {
       
       var link = $(e.relatedTarget);
       var dvdId = link.data("dvdid");
       
       $.ajax({
            url: contextRoot + "/dvd/" + dvdId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {

                $("#editId").val(data.id);
                $("#editTitle").val(data.title);
                $("#editReleaseDate").val(data.releaseDate);
                $("#editRated").val(data.rated);
                $("#editDirector").val(data.director);
                $("#editStudio").val(data.studio);                
            },
            error: function(data, status) {
                alert("error");
            }            
       });
   });
   
    $(document).on("click", ".delete-link", function(e) {
       
      e.preventDefault();
      var dvdId = $(e.target).data("dvdid");      
      console.log("id: " + dvdId);
      
      $.ajax({
          type: "DELETE",
          url: contextRoot + "/dvd/" + dvdId,
          success: function(data, status) {
              $("#dvdRow-" + dvdId).remove();
          },
          error: function(data, status) {
              alert("There was an issue deleting the dvd " + dvdId + "! \n"
                + status);
          }
      });
   });
    
    function buildDvdRow(data) {
        return "<tr id='dvdRow-" + data.id + "'>  \n\
                <td><a data-dvdid='" + data.id +"' data-toggle='modal' data-target='#showDvdModal'>" + data.title + "</a></td>  \n\
                <td> " + data.releaseDate + "</td>    \n\
                <td> " + data.rated + "</td>    \n\
                <td> <a data-dvdid='" + data.id +"' data-toggle='modal' data-target='#editDvdModal'><i class='glyphicon glyphicon-pencil'></i></a>  </td>   \n\
                <td> <a data-dvdid='" + data.id +"' class='delete-link'><i class='glyphicon glyphicon-remove'></i></a>  </td>   \n\
                </tr>  ";
    }
    
    function buildNoteRow(data) {
        console.log(data);
        return "<tr id='noteRow-" + data.noteId + "'>  \n\
                <td> " + data.noteId + "</td>    \n\
                <td> " + data.noteText + "</td>    \n\
                <td></td>   \n\
                <td> <a data-noteid='" + data.noteId +"' class='delete-link-notes'>Delete</a>  </td>   \n\
                </tr>  ";
    }
        
    function setValuesForShowModal(data) {
        $("#showDvdId").val(data.id);
        $("#showTitle").text(data.title);
        $("#showReleaseDate").text(data.releaseDate);
        $("#showRated").text(data.rated);
        $("#showDirector").text(data.director);
        $("#showStudio").text(data.studio);        
    }
});


