/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    
    $("#submitCreateNote").on("click", function(e) {
       
        e.preventDefault();
        
        var noteData = JSON.stringify({           
           dvdId: $("#showDvdId").val(),
           noteText: $("#txtAddNote").val(),
           //creationDate: "2016-06-16"           
        });
        
        console.log(noteData);
        
        $.ajax({
           url: contextRoot + "/note/",
           type: "POST",
           data: noteData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
                var noteRow = buildNoteRow(data);
                $("#noteTable").append($(noteRow));
                
                $("#txtAddNote").val("");
                
          },
          error: function(data, status) {
              
              var errors = data.responseJSON.errors;
              
              $.each(errors, function(index, error) {                  
                  console.log(error.fieldName + ": " + error.message);                
              });
          }
        });
    });    
    
    $(document).on("click", ".delete-link-notes", function(e) {
       
      e.preventDefault();
      var noteId = $(e.target).data("noteid");      
      console.log("id: " + noteId);
      
      $.ajax({
          type: "DELETE",
          url: contextRoot + "/note/" + noteId,
          success: function(data, status) {
              $("#noteRow-" + noteId).remove();
          },
          error: function(data, status) {
              alert("There was an issue deleting the note " + noteId + "! \n"
                + status);
          }
      });
   });
    
    function buildNoteRow(data) {
        return "<tr id='noteRow-" + data.noteId + "'>  \n\
                <td> " + data.noteId + "</td>    \n\
                <td> " + data.noteText + "</td>    \n\
                <td></td>   \n\
                <td> <a data-noteid='" + data.noteId +"' class='delete-link-notes'>Delete</a>  </td>   \n\
                </tr>  ";
    }
});


