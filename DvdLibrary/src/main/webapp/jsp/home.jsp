<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dvd Library Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <!--<link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">-->
        <link href="${pageContext.request.contextPath}/css/dvdlibrary.css" rel="stylesheet">
        
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container col-md-12 cont-background" style="padding-right: 5px">
            <h1 class="header">Dvd Library</h1>            
            <div class="navbar navbar-background">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}" style="color: black;">Home</a></li>
                </ul>    
            </div>
            <!--<div class="col-md-1"></div>-->
            <div class="col-md-12">
                <div class="row row-background">                
                    <div class="col-md-6" style="background-color: whitesmoke; border: 1px solid #666666; padding-bottom: 3px; height: 475px">
                        <h3 class="header">Dvd List</h3>
                        <div class="pageContent" style="height: 350px; overflow-y: scroll">                        
                            <table class="table table-bordered" id="dvdTable">
                                <thead>
                                    <tr>
                                        <th>DVD Title</th>
                                        <th>Release Date</th>
                                        <th>MPAA Rating</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${dvdList}" var="dvd">
                                        <tr id="dvdRow-${dvd.id}">
                                            <td><a data-dvdid="${dvd.id}" data-toggle="modal" data-target="#showDvdModal">${dvd.title}</a></td>
                                            <td>${dvd.releaseDate}</td>
                                            <td>${dvd.rated}</td>
                                            <td><a data-dvdid="${dvd.id}" data-toggle="modal" data-target="#editDvdModal"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                            <td><a data-dvdid="${dvd.id}" class="delete-link">Delete</a></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <form class="form-horizontal">
                            <div class="pageContent" style="margin-top: 5px; padding: 5px">                                
                                <label for="filterValue">Filter Value</label>
                                <input type="text" name="filterValue" id="filterValue" />
                                <input type="submit" id="submitDvdFilter" value="Filter" />
                            </div>
                        </form>
                    </div>
                            <div class="col-md-1"></div>
                    <div class="col-md-5" style="background-color: whitesmoke; border: 1px solid #666666; height: 475px">
                        <h3 class="header" style="width:100%">Create New DVD Entry</h3>
                        <div class="pageContent" style="height: 400px">                                
                            <form method="POST" class="form-horizontal">                            
                                <div class="form-group">
                                    <label for="dvdTitle" class="col-md-4 control-label">DVD Title:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="dvdTitle" id="dvdTitle" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnDvdTitle" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="dvdReleaseDate" class="col-md-4 control-label">Release Date:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="dvdReleaseDate" id="dvdReleaseDate" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnReleaseDate" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="dvdRated" class="col-md-4 control-label">MPAA Rating:</label>
                                    <div class="col-md-8">
                                        <select name="dvdRated" id="dvdRated">
                                            <option value=""></option>
                                            <option value="G">G</option>
                                            <option value="PG">PG</option>
                                            <option value="PG-13">PG-13</option>
                                            <option value="R">R</option>
                                            <option value="NC-17">NC-17</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnRated" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="dvdDirector" class="col-md-4 control-label">Director:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="dvdDirector" id="dvdDirector" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnDirector" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="dvdStudio" class="col-md-4 control-label">Studio:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="dvdStudio" id="dvdStudio" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnStudio" class="col-md-8 warning"></div>
                                </div>
                                <div class="form-group">
                                    <label for="dvdNote" class="col-md-4 control-label">Note:</label>
                                    <div class="col-md-8">
                                        <textarea id="txtDvdNote" name="txtDvdNote"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <input type="submit" id="submitCreate" value="Create DVD Entry" class="btn btn-primary"/> 
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>            
            <div class="footer">
                Created by James Svitak
            </div>
        </div>
                            
        <div id="showDvdModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">DVD Summary</h4>
                    </div>
                    <div class="modal-body">
                        <div id="showModalSuccessHead" style="display:none">
                            <h6 style='color: green; font-weight: bold'>DVD Successfully Created</h6>
                        </div>
                        <div>
                            <input type="hidden" id="showDvdId" />
                            <table class="table table-bordered" id="showDvdTable">
                                <tr>
                                    <th>Title:</th>
                                    <td id="showTitle"></td>
                                </tr>
                                <tr>
                                    <th>Release Date:</th>
                                    <td id="showReleaseDate"></td>
                                </tr>
                                <tr>
                                    <th>MPAA Rating</th>
                                    <td id="showRated"></td>
                                </tr>
                                <tr>
                                    <th>Director:</th>
                                    <td id="showDirector"></td>
                                </tr>
                                <tr>
                                    <th>Studio:</th>
                                    <td id="showStudio"></td>
                                </tr>                            
                            </table>
                        </div>
                        <div>
                            <table class="table table-bordered" id="noteTable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Note Text</th>
                                        <th>Note Date</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody id="noteTableBody"></tbody>
                            </table>
                        </div>
                        <div>
                            <div class="col-md-8">
                                <textarea id="txtAddNote" style="width: 99%"></textarea>
                            </div>
                            <div class="col-md-4">
                                <input type="submit" id="submitCreateNote" value="Add Note" />
                            </div>                                        
                        </div>
                    </div>                    
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="editDvdModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update DVD Info</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="editId" />
                        <table class="table table-bordered" id="editDvdTable">
                            <input type="hidden" id="editAddressId" />
                            <tr>
                                <th>Title:</th>
                                <td>
                                    <input type="text" id="editTitle" />
                                </td>
                            </tr>
                            <tr>
                                <th>Release Date:</th>
                                <td> 
                                    <input type="text" id="editReleaseDate" />
                                </td>
                            </tr>
                            <tr>
                                <th>MPAA Rating:</th>
                                <td> 
                                    <input type="text" id="editRated" />
                                </td>
                            </tr>
                            <tr>
                                <th>Director:</th>
                                <td>
                                    <input type="text" id="editDirector" />
                                </td>
                            </tr>
                            <tr>
                                <th>Studio:</th>
                                <td> 
                                    <input type="text" id="editStudio" />
                                </td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" id="submitEdit">Save</button>
                    </div>
                </div>
            </div>
        </div>        
        
        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>                    
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>
        <script src="${pageContext.request.contextPath}/js/note.js"></script>

    </body>
</html>

