<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dvd Library Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Dvd Library</h1>
            <hr/>
            <div class="navbar">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>
                </ul>    
            </div>
            <div class="row">                
                <div class="col-md-6">
                    <form method="POST" action="./" class="form-horizontal">
                        <input type="hidden" name="id" value="${dvd.id}" />
                        <div class="form-group">
                            <label for="title" class="col-md-4 control-label">DVD Title:</label>
                            <div class="col-md-8">
                                <input type="text" name="title" id="title" value="${dvd.title}" />
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="releaseDate" class="col-md-4 control-label">Release Date:</label>
                            <div class="col-md-8">
                                <input type="text" name="releaseDate" id="releaseDate" value="${dvd.releaseDate}" />
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="rated" class="col-md-4 control-label">MPAA Rating:</label>
                            <div class="col-md-8">
                                <input type="text" name="rated" id="rated" value="${dvd.rated}" />
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="director" class="col-md-4 control-label">Director:</label>
                            <div class="col-md-8">
                                <input type="text" name="director" id="director" value="${dvd.director}" />
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="studio" class="col-md-4 control-label">Studio:</label>
                            <div class="col-md-8">
                                <input type="text" name="studio" id="studio" value="${dvd.studio}" />
                            </div>
                        </div>
                        <input type="submit" value="Submit Query" class="btn btn-default pull-right"/>
                    </form>
                </div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

