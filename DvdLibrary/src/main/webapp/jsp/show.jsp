<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>                
                </ul>    
            </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="divTitle" class="col-md-4 control-label">DVD Title:</label>
                            <div id="divTitle">
                                ${dvd.title}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="divReleaseDate" class="col-md-4 control-label">Release Date:</label>
                            <div id="divReleaseDate">
                                ${dvd.releaseDate}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="divRating" class="col-md-4 control-label">Rating:</label>
                            <div id="divRating">
                                ${dvd.rated}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="divDirector" class="col-md-4 control-label">Director:</label>
                            <div id="divDirector">
                                ${dvd.director}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="divStudio" class="col-md-4 control-label">Studio:</label>
                            <div id="divStudio">
                                ${dvd.studio}
                            </div>
                        </div>                        
                    </div>
                </div>                
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

