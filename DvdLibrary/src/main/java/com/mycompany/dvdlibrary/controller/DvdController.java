/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.controller;

import com.mycompany.dvdlibrary.dao.DvdDao;
import com.mycompany.dvdlibrary.dto.Dvd;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/dvd")
public class DvdController {
    
    private DvdDao dvdDao;
    private List<Dvd> dvdList;
    
    @Inject
    public DvdController(DvdDao dao) {
        this.dvdDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.POST)
    @ResponseBody
    public Dvd create(@Valid @RequestBody Dvd dvd) {
        
        return dvdDao.create(dvd);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer dvdId) {
        Dvd objDvd = dvdDao.getById(dvdId);
        dvdDao.delete(objDvd);        
    }
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public String edit(@PathVariable("id") Integer dvdId, Map model) {
        
        Dvd objDvd = dvdDao.getById(dvdId);
        model.put("dvd", objDvd);
        return "edit";
    }
    
    @RequestMapping(value="/", method=RequestMethod.PUT)
    @ResponseBody
    public Dvd editSubmit(@Valid @RequestBody Dvd objDvd) {
        
        dvdDao.update(objDvd);
        return objDvd;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Dvd show(@PathVariable("id") Integer dvdId) {
        
        Dvd objDvd = dvdDao.getById(dvdId);       
        return objDvd;
    }
    
    @RequestMapping(value="/filter/{filterValue}", method=RequestMethod.POST)
    @ResponseBody
    public List<Dvd> filterDvdList(@PathVariable("filterValue") String filterValue) {
               
        dvdList = new ArrayList();
        List<Dvd> returnList = new ArrayList();
        
        returnList.add(dvdDao.get(filterValue));
        dvdList = addToFilterList(returnList);
        
        returnList = dvdDao.getByRating(filterValue);
        dvdList = addToFilterList(returnList);
        
        returnList = dvdDao.getByStudio(filterValue);
        dvdList = addToFilterList(returnList);
        
        return dvdList;
    }
    
    public List<Dvd> addToFilterList(List<Dvd> filterList) {
        for (Dvd dvd : filterList) {
            dvdList.add(dvd);
        }
        
        return dvdList;
    }    
}
