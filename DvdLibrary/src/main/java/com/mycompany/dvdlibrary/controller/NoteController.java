/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.controller;

import com.mycompany.dvdlibrary.dao.NoteDao;
import com.mycompany.dvdlibrary.dto.Note;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/note")
public class NoteController {
    
    private NoteDao noteDao;
    private List<Note> noteList = new ArrayList();
    
    @Inject
    public NoteController(NoteDao dao) {
        this.noteDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.POST)
    @ResponseBody
    public Note create(@Valid @RequestBody Note note) {
        
        return noteDao.create(note);
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer noteId) {
        Note objNote = noteDao.getById(noteId);
        noteDao.delete(objNote);        
    }
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public String edit(@PathVariable("id") Integer noteId, Map model) {
        
        Note objNote = noteDao.getById(noteId);
        model.put("note", objNote);
        return "edit";
    }
    
    @RequestMapping(value="/", method=RequestMethod.PUT)
    @ResponseBody
    public Note editSubmit(@Valid @RequestBody Note objNote) {
        
        noteDao.update(objNote);
        return objNote;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Note show(@PathVariable("id") Integer noteId) {
        
        Note objNote = noteDao.getById(noteId);       
        return objNote;
    }
    
    @RequestMapping(value="/dvdnotes/{dvdid}", method=RequestMethod.GET)
    @ResponseBody
    public List<Note> getDvdNotes(@PathVariable("dvdid") Integer dvdId) {
        
        List<Note> noteList = noteDao.getNotesForDvd(dvdId);
        return noteList;
    }
    
    @RequestMapping(value="/avgcount", method=RequestMethod.GET)
    @ResponseBody
    public float getAvgNoteCount() {
        return noteDao.getAverageNoteCount(0);
    }
}
