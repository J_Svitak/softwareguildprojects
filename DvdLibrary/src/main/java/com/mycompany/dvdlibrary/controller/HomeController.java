package com.mycompany.dvdlibrary.controller;

import com.mycompany.dvdlibrary.dao.DvdDao;
import com.mycompany.dvdlibrary.dao.NoteDao;
import com.mycompany.dvdlibrary.dto.Dvd;
import com.mycompany.dvdlibrary.dto.Note;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
    
    private DvdDao dvdDao;
    private NoteDao noteDao;
    private List<Dvd> dvdList = new ArrayList();
    private List<Note> noteList = new ArrayList();
    
    @Inject
    public HomeController(DvdDao dvdDao, NoteDao noteDao) {
        this.dvdDao = dvdDao;
        this.noteDao = noteDao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String loadDvdLibrary(Map<String, Object> model) {
        
        List<Dvd> dvdList = dvdDao.getDvdList();
        boolean isDvdListEmpty = false;
        
        if (dvdList.size() == 0) {
            isDvdListEmpty = true;
        }
        
        noteList = noteDao.getNoteList();
        
        model.put("isDvdListEmpty", isDvdListEmpty);
        //model.put("noteList", noteList);
        model.put("dvdList", dvdList);
        return "home";
    }
    
    @RequestMapping(value="/filter", method=RequestMethod.POST)
    public String filterAddressList(@RequestParam("filterBy") String selectedOption, @RequestParam("filterValue") String filterValue, Map model) {
        
        dvdList = new ArrayList();
                
        switch(selectedOption) {
            case "title":
                Dvd objDvd = dvdDao.get(filterValue);
                
                if (objDvd != null) {
                    dvdList.add(objDvd);
                }                
                break;
            case "rating":
                dvdList = dvdDao.getByRating(filterValue);
                break;
            case "director":
                //dvdList = dvdDao.getByDirector(filterValue);
                break;
            case "studio":
                dvdList = dvdDao.getByStudio(filterValue);
                break;
            case "releaseDate":
                dvdList = dvdDao.getByReleaseDate(Integer.parseInt(filterValue));
            default:
                dvdList = dvdDao.getDvdList();
                break;
        }
        
        model.put("dvdList", dvdList);
        return "home";
    }
}
