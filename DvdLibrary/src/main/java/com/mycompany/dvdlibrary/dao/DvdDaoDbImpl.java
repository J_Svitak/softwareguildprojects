/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dao;

import com.mycompany.dvdlibrary.dto.Dvd;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class DvdDaoDbImpl implements DvdDao {
    
    private List<Dvd> dvdList;
    private JdbcTemplate jdbcTemplate;
    
    private static final String SQL_GET_DVD_BY_ID = "SELECT * "
            + "FROM dvd d "
            + "LEFT JOIN ratings r ON r.id = d.rating_id "
            + "WHERE d.id = ?";
    private static final String SQL_GET_DVD_BY_TITLE= "SELECT * "
            + "FROM dvd d "
            + "LEFT JOIN ratings r ON r.id = d.rating_id "
            + "WHERE d.title = ?";
    private static final String SQL_GET_DVD_LIST = "SELECT * "
            + "FROM dvd d "
            + "LEFT JOIN ratings r ON r.id = d.rating_id "
            + "ORDER BY d.title, d.release_date";
    private static final String SQL_GET_BY_RATING = "SELECT * "
            + "FROM dvd d "
            + "INNER JOIN ratings r ON r.id = d.rating_id "
            + "WHERE r.mpaa_rating = ?";
    private static final String SQL_GET_BY_STUDIO = "SELECT * "
            + "FROM dvd d "
            + "INNER JOIN ratings r ON r.id = d.rating_id "
            + "WHERE d.studio = ?";
    private static final String SQL_GET_MIN_RELEASE_DATE = "SELECT d.* "
            + "FROM dvd d INNER JOIN"
            + "(SELECT MIN(release_date) release_date FROM dvd "
            + ") d2 ON d2.release_date = d.release_date";
    private static final String SQL_GET_MAX_RELEASE_DATE = "SELECT d.* "
            + "FROM dvd d INNER JOIN"
            + "(SELECT MIN(release_date) release_date FROM dvd "
            + ") d2 ON d2.release_date = d.release_date";
    private static final String SQL_GET_DVD_AGE = "SELECT id, EXTRACT(year FROM curdate()) - EXTRACT(YEAR FROM release_date) as 'age' "
            + "FROM dvd ORDER BY age DESC";
    private static final String SQL_GET_DVD_AVG_AGE = "SELECT id, AVG(EXTRACT(year FROM curdate()) - EXTRACT(YEAR FROM release_date)) "
            + "FROM dvd";
    
    private static final String SQL_CREATE_DVD = "INSERT INTO dvd "
            + "(title, release_date, rating_id, director, studio) "
            + "VALUES (?, ?, (SELECT id FROM ratings WHERE mpaa_rating = ?), ?, ?)";
    
    private static final String SQL_UPDATE_DVD = "UPDATE dvd "
            + "SET title = ?, release_date = ?, rating_id = (SELECT id FROM ratings WHERE mpaa_rating = ?), director = ?, studio = ? "
            + "WHERE id = ?";
    
    private static final String SQL_DELETE_DVD = "DELETE FROM dvd "
            + "WHERE id = ?";
    private static final String SQL_DELETE_DVD_NOTES = "DELETE FROM notes "
            + "WHERE dvd_id = ?";
    
    
    public DvdDaoDbImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Dvd create(Dvd objDvd) {
        
        String rated = objDvd.getRated();
        
        jdbcTemplate.update(SQL_CREATE_DVD,
                objDvd.getTitle(),
                objDvd.getReleaseDate(),
                objDvd.getRated(),
                objDvd.getDirector(),
                objDvd.getStudio()
        );
        
        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        objDvd.setId(id);
        
        return objDvd;
    }
    
    @Override
    public Dvd get(String dvdTitle) {
        
        String sqlCount = "SELECT COUNT(*) "
                + "FROM dvd WHERE title = ?";
        
        int rowCount = jdbcTemplate.queryForObject(sqlCount, Integer.class, dvdTitle);
        if (rowCount > 0) {
            return jdbcTemplate.queryForObject(SQL_GET_DVD_BY_TITLE, new DvdMapper(), dvdTitle);
        }
        
        return null;
    }
    
    @Override 
    public Dvd getById(int id) {
        
        Dvd dvd = new Dvd();
        dvd = jdbcTemplate.queryForObject(SQL_GET_DVD_BY_ID, new DvdMapper(), id);
        return dvd;
        
    }
    
    @Override
    public List<Dvd> getByReleaseDate(int yearDif) {
        
        List<Dvd> dvdMatches = new ArrayList();
        Calendar targetCalendar = Calendar.getInstance();        
        targetCalendar.add(Calendar.YEAR, -yearDif);                
        Date minDate = targetCalendar.getTime();
        
        for (Dvd objDvd : dvdList) {
            
            /*if (objDvd.getReleaseDate().after(minDate) || objDvd.getReleaseDate() == minDate) {
                dvdMatches.add(objDvd);
            }*/
        }
        
        return dvdMatches;
    }
    
    @Override
    public List<Dvd> getByRating(String rating) {
        List<Dvd> dvdsByRating = new ArrayList();
        dvdsByRating = jdbcTemplate.query(SQL_GET_BY_RATING, new DvdMapper(), rating);
        return dvdsByRating;
    }
    
    @Override
    public Map<String, List<Dvd>> getByDirector(String director) {
        
        Map<String, List<Dvd>> dvdDirectorMap = new HashMap();
        
        for (Dvd objDvd : dvdList) {
            List<Dvd> dvdResultList = new ArrayList();
            Set<String> directorSet = dvdDirectorMap.keySet();
            
            if (director.equals(objDvd.getDirector())) {
                if (directorSet.contains(director)) {
                    dvdResultList = dvdDirectorMap.get(director);
                }
                
                dvdResultList.add(objDvd);
                dvdDirectorMap.put(director, dvdResultList);
            }
        }
        
        return dvdDirectorMap;
    }
    
    @Override
    public List<Dvd> getByStudio(String studio) {
        
        return jdbcTemplate.query(SQL_GET_BY_STUDIO, new DvdMapper(), studio);
    }
    
    @Override
    public double getAverageAge() {
        
        return jdbcTemplate.queryForObject(SQL_GET_DVD_AVG_AGE, Double.class);
    }
    
    @Override
    public Dvd getNewestMovie() {
                       
        return jdbcTemplate.queryForObject(SQL_GET_MAX_RELEASE_DATE, new DvdMapper());
    }
    
    @Override
    public Dvd getOldestMovie() {
        
        return jdbcTemplate.queryForObject(SQL_GET_MIN_RELEASE_DATE, new DvdMapper());
    }
    
    @Override
    public void update(Dvd objDvd) {
        
        jdbcTemplate.update(SQL_UPDATE_DVD,
                objDvd.getTitle(),
                objDvd.getReleaseDate(),
                objDvd.getRated(),
                objDvd.getDirector(),
                objDvd.getStudio(),
                objDvd.getId());
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Dvd objDvd) {
        
        jdbcTemplate.update(SQL_DELETE_DVD_NOTES, objDvd.getId());
        jdbcTemplate.update(SQL_DELETE_DVD, objDvd.getId());
    }
    
    @Override
    public List<Dvd> getDvdList() {
        return jdbcTemplate.query(SQL_GET_DVD_LIST, new DvdMapper());
    }
    
    private static final class DvdMapper implements RowMapper<Dvd> {

        @Override
        public Dvd mapRow(ResultSet rs, int i) throws SQLException {
            
            Dvd dvd = new Dvd();
            
            dvd.setId(rs.getInt("id"));
            dvd.setTitle(rs.getString("title"));
            dvd.setReleaseDate(rs.getDate("release_date"));
            dvd.setRated(rs.getString("mpaa_rating"));
            dvd.setDirector(rs.getString("director"));
            dvd.setStudio(rs.getString("studio"));
            
            return dvd;
        }
    }
    
    @Override
    public void encode() {
                
    }
    
    @Override
    public List<Dvd> decode() {
        dvdList = getDvdList();
        return dvdList;
    }    
}
