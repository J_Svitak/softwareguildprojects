/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dao;

import com.mycompany.dvdlibrary.dto.Note;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class NoteDaoDbImpl implements NoteDao {
    
    private List<Note> noteList;
    private JdbcTemplate jdbcTemplate;
    
    private static final String SQL_GET_DVD_NOTES = "SELECT * "
            + "FROM notes "
            + "WHERE dvd_id = ?";
    private static final String SQL_GET_NOTE_BY_ID = "SELECT * "
            + "FROM notes "
            + "WHERE id = ?";
    private static final String SQL_GET_NOTES_LIST = "SELECT * "
            + "FROM notes";            
    
    private static final String SQL_CREATE_DVD_NOTE = "INSERT INTO notes "
            + "(dvd_id, note_text, creation_date) "
            + "VALUES (?, ?, '2016-06-16')";
    
    private static final String SQL_DELETE_NOTE = "DELETE FROM notes "
            + "WHERE id = ?";
    
    private static final String SQL_UPDATE_NOTE = "UPDATE notes "
            + "SET note_text = ? "
            + "WHERE id = ?";
    
    private static final String SQL_GET_AVG_NOTE_COUNT = "SELECT AVG( "
            + "(SELECT COUNT(*) FROM notes) / "
            + "(SELECT COUNT(*) FROM dvd))";
    
    
    public NoteDaoDbImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        noteList = getNoteList();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Note create(Note objNote) {
        
        jdbcTemplate.update(SQL_CREATE_DVD_NOTE,
                objNote.getDvdId(), 
                objNote.getNoteText()                
        );
        
        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        objNote.setNoteId(id);
        
        return objNote;
    }
    
    @Override
    public Note getById(int noteId) {
        
        return jdbcTemplate.queryForObject(SQL_GET_NOTE_BY_ID, new NoteMapper(), noteId);
    }
    
    @Override
    public Note getByText(String noteText) {
        
        for (Note objNote : noteList) {
            if (noteText.equals(objNote.getNoteText())) {
                return objNote;
            }
        }
        
        return null;
    }
    
    @Override
    public List<Note> getNotesForDvd(int dvdId) {
        
        List<Note> dvdNotes = new ArrayList();                
        dvdNotes = jdbcTemplate.query(SQL_GET_DVD_NOTES, new NoteMapper(), dvdId);        
        return dvdNotes;
    }
    
    @Override
    public void update(Note objNote) {
        
        jdbcTemplate.update(SQL_UPDATE_NOTE, objNote.getNoteText(), objNote.getNoteId());
    }
    
    @Override
    public void delete(Note objNote) {
        
        jdbcTemplate.update(SQL_DELETE_NOTE, objNote.getNoteId());
    }
        
    @Override
    public List<Note> getNoteList() {
        return jdbcTemplate.query(SQL_GET_NOTES_LIST, new NoteMapper());
    }
    
    @Override
    public float getAverageNoteCount(int dvdCount) {
        
        float avgNoteCount = 0f;        
        avgNoteCount = jdbcTemplate.queryForObject(SQL_GET_AVG_NOTE_COUNT, Float.class);
        return avgNoteCount;
    }
    
    public static final class NoteMapper implements RowMapper<Note> {

        @Override
        public Note mapRow(ResultSet rs, int i) throws SQLException {
            
            Note note = new Note();
            
            note.setNoteId(rs.getInt("id"));
            note.setDvdId(rs.getInt("dvd_id"));
            note.setNoteText(rs.getString("note_text"));
            
            return note;
        }
        
    }
    
    @Override
    public void encode() {
        
    }
    
    @Override
    public List<Note> decode() {
        
        noteList = getNoteList();
        return noteList;
    }
}
