/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dao;

import com.mycompany.dvdlibrary.dto.Note;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class NoteDaoImpl implements NoteDao {
    
    private int nextId = 101;
    private List<Note> noteList;
    private final String filename = "dvdNotes.txt";
    private final String TOKEN = "::";
    
    public NoteDaoImpl() {
        noteList = new ArrayList();
        decode();
    }
    
    @Override
    public Note create(Note objNote) {
        
        objNote.setNoteId(nextId);
        nextId++;
        
        noteList.add(objNote);
        encode();
        return objNote;
    }
    
    @Override
    public Note getById(int noteId) {
        
        for (Note objNote : noteList) {
            if (noteId == objNote.getNoteId()) {
                return objNote;
            }
        }
        
        return null;
    }
    
    @Override
    public Note getByText(String noteText) {
        
        for (Note objNote : noteList) {
            if (noteText.equals(objNote.getNoteText())) {
                return objNote;
            }
        }
        
        return null;
    }
    
    @Override
    public List<Note> getNotesForDvd(int dvdId) {
        
        List<Note> dvdNotes = new ArrayList();
                
        for (Note objNote : noteList) {
            if (dvdId == objNote.getDvdId()) {
                dvdNotes.add(objNote);
            }
        }
        
        return dvdNotes;
    }
    
    @Override
    public void update(Note objNote) {
        
        for (Note myNote : noteList) {
            if (myNote.getNoteId()== objNote.getNoteId()) {
                noteList.remove(myNote);
                noteList.add(objNote);
                break;
            }
        }
        
        encode();
    }
    
    @Override
    public void delete(Note objNote) {
        
        for (Note myNote : noteList) {
            if (myNote.getNoteId()== objNote.getNoteId()) {
                noteList.remove(myNote);                
                break;
            }
        }
        encode();
    }
        
    @Override
    public List<Note> getNoteList() {
        return noteList;
    }
    
    @Override
    public float getAverageNoteCount(int dvdCount) {
        
        float avgNoteCount = 0f;        
        avgNoteCount = (float)noteList.size() / dvdCount;        
        return avgNoteCount;
    }
    
    @Override
    public void encode() {
        
        try {            
            PrintWriter out = new PrintWriter(new FileWriter(filename));
            
            for (Note objNote : noteList) {                
                
                out.print(objNote.getNoteId()+ TOKEN);                
                out.print(objNote.getDvdId()+ TOKEN);                
                out.print(objNote.getNoteText()+ TOKEN);                                
                out.println();
            }            
            
            out.flush();
            out.close();
        } 
        catch (IOException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public List<Note> decode() {
        
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(filename)));
            
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");                
                
                Note objNote = new Note();
                
                int noteId = Integer.parseInt(stringParts[0]);
                int dvdId = Integer.parseInt(stringParts[1]);                
                String noteText = stringParts[2];
                
                objNote.setNoteId(noteId);
                objNote.setDvdId(dvdId);
                objNote.setNoteText(noteText);                
                
                noteList.add(objNote);
                
                if (noteId >= nextId) {
                    nextId = noteId + 1;
                }
            }
        } 
        catch (FileNotFoundException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
        return noteList;
    }
}
