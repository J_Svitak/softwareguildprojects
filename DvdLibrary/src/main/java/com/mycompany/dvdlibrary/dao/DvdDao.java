/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dao;

import com.mycompany.dvdlibrary.dto.Dvd;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DvdDao {
    public Dvd create(Dvd objDvd);
    public Dvd get(String dvdTitle);
    public Dvd getById(int id);
    public List<Dvd> getByReleaseDate(int dt);
    public List<Dvd> getByRating(String rating);
    public Map<String, List<Dvd>> getByDirector(String director);
    public List<Dvd> getByStudio(String studio);
    public double getAverageAge();
    public Dvd getNewestMovie();
    public Dvd getOldestMovie();
    //public Note getAverageNoteCount();
    public void update(Dvd dvdToUpdate);
    public void delete(Dvd dvdToDelete);
    public List<Dvd> getDvdList();
    public void encode();
    public List<Dvd> decode();
}
