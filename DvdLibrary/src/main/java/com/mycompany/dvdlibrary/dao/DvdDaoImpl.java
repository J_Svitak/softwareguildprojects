/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dao;

import com.mycompany.dvdlibrary.dto.Dvd;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author apprentice
 */
public class DvdDaoImpl implements DvdDao {
    private int nextId = 101;
    private List<Dvd> dvdList;
    private final String filename = "dvdLibrary.txt";
    private final String TOKEN = "::";
    
    public DvdDaoImpl() {
        dvdList = new ArrayList();
        dvdList = decode();
    }
    
    @Override
    public Dvd create(Dvd objDvd) {
        
        objDvd.setId(nextId);
        nextId++;
        
        dvdList.add(objDvd);
        encode();
        return objDvd;
    }
    
    @Override
    public Dvd get(String dvdTitle) {
                
        for (Dvd objDvd : dvdList) {
            if (dvdTitle.equals(objDvd.getTitle())) {
                return objDvd;
            }
        }
        
        return null;
    }
    
    @Override 
    public Dvd getById(int id) {
        
        for (Dvd objDvd : dvdList) {
            if (objDvd.getId() == id) {
                return objDvd;
            }
        }
        
        return null;
    }
    
    @Override
    public List<Dvd> getByReleaseDate(int yearDif) {
        
        List<Dvd> dvdMatches = new ArrayList();
        Calendar targetCalendar = Calendar.getInstance();        
        targetCalendar.add(Calendar.YEAR, -yearDif);                
        Date minDate = targetCalendar.getTime();
        
        for (Dvd objDvd : dvdList) {
            
            /*if (objDvd.getReleaseDate().after(minDate) || objDvd.getReleaseDate() == minDate) {
                dvdMatches.add(objDvd);
            }*/
        }
        
        return dvdMatches;
    }
    
    @Override
    public List<Dvd> getByRating(String rating) {
        
        List<Dvd> dvdResultList = new ArrayList();
        
        for (Dvd objDvd : dvdList) {
            if (rating.equals(objDvd.getRated())) {
                dvdResultList.add(objDvd);
            }
        }
        
        return dvdResultList;
    }
    
    @Override
    public Map<String, List<Dvd>> getByDirector(String director) {
        
        Map<String, List<Dvd>> dvdDirectorMap = new HashMap();
        
        for (Dvd objDvd : dvdList) {
            List<Dvd> dvdResultList = new ArrayList();
            Set<String> directorSet = dvdDirectorMap.keySet();
            
            if (director.equals(objDvd.getDirector())) {
                if (directorSet.contains(director)) {
                    dvdResultList = dvdDirectorMap.get(director);
                }
                
                dvdResultList.add(objDvd);
                dvdDirectorMap.put(director, dvdResultList);
            }
        }
        
        return dvdDirectorMap;
    }
    
    @Override
    public List<Dvd> getByStudio(String studio) {
        
        List<Dvd> dvdResultList = new ArrayList();
        
        for (Dvd objDvd : dvdList) {
            if (studio.equals(objDvd.getStudio())) {
                dvdResultList.add(objDvd);
            }
        }
        
        return dvdResultList;
    }
    
    @Override
    public double getAverageAge() {
        
        double avgAge = 0;
        double totalAge = 0;
        Date currentDate = new Date();
        
        for (Dvd objDvd : dvdList) {
            //totalAge = currentDate.getTime() - objDvd.getReleaseDate().getTime();
        }
        
        avgAge = totalAge / dvdList.size();
        
        double milisecsInYear = 1000 * 60 * 60 * 24;        
        return avgAge / (milisecsInYear * 365);
    }
    
    @Override
    public Dvd getNewestMovie() {
                       
        Dvd newestDvd = dvdList.get(0);        
        
        for (Dvd objDvd : dvdList) {
            /*if (objDvd.getReleaseDate().after(newestDvd.getReleaseDate())) {
                newestDvd = objDvd;
            }*/
        }
        
        return newestDvd;
    }
    
    @Override
    public Dvd getOldestMovie() {
        
        Dvd oldestDvd = dvdList.get(0);        
        
        for (Dvd objDvd : dvdList) {
            /*if (objDvd.getReleaseDate().before(oldestDvd.getReleaseDate())) {
                oldestDvd = objDvd;
            }*/
        }
        
        return oldestDvd;
    }
    
    @Override
    public void update(Dvd objDvd) {
        
        for (Dvd myDvd : dvdList) {
            if (myDvd.getId() == objDvd.getId()) {
                dvdList.remove(myDvd);
                dvdList.add(objDvd);
                break;
            }
        }
        
        encode();
    }
    
    @Override
    public void delete(Dvd objDvd) {
        for (Dvd myDvd : dvdList) {
            if (myDvd.getId() == objDvd.getId()) {
                dvdList.remove(myDvd);                
                break;
            }
        }
        
        encode();
    }
    
    @Override
    public List<Dvd> getDvdList() {
        return dvdList;
    }
    
    @Override
    public void encode() {        
        
        try {            
            PrintWriter out = new PrintWriter(new FileWriter(filename));
            
            for (Dvd objDvd : dvdList) {                
                
                out.print(objDvd.getId() + TOKEN);                
                out.print(objDvd.getTitle() + TOKEN);
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                String releaseDateString = dateFormat.format(objDvd.getReleaseDate());
                //String releaseDateString = objDvd.getReleaseDate();
                out.print(releaseDateString + TOKEN);                
                
                out.print(objDvd.getRated() + TOKEN);                
                out.print(objDvd.getDirector() + TOKEN);                
                out.print(objDvd.getStudio() + TOKEN);
                out.println();
            }            
            
            out.flush();
            out.close();
        } 
        catch (IOException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    @Override
    public List<Dvd> decode() {
                
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(filename)));
            
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");                
                
                Dvd objDvd = new Dvd();
                
                int id = Integer.parseInt(stringParts[0]);
                String title = stringParts[1];
                
                String releaseDateString = stringParts[2];
                SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
                Date releaseDate;// = new Date();
                releaseDate = sdf.parse(releaseDateString);
                
                String rated = stringParts[3];
                String director = stringParts[4];
                String studio = stringParts[5];                
                
                objDvd.setId(id);
                objDvd.setTitle(title);
                objDvd.setReleaseDate(releaseDate);
                objDvd.setRated(rated);
                objDvd.setDirector(director);
                objDvd.setStudio(studio);
                
                dvdList.add(objDvd);
                
                if (id >= nextId) {
                    nextId = id + 1;
                }
            }
        } 
        catch (FileNotFoundException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(DvdDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return dvdList;
    }    
}
