/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dao;

import com.mycompany.dvdlibrary.dto.Note;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface NoteDao {
    public Note create(Note objNote);
    public Note getById(int noteId);
    public Note getByText(String noteText);
    public void update(Note objNote);
    public void delete(Note objNote);
    public List<Note> getNoteList();
    public List<Note> getNotesForDvd(int dvdId);
    public float getAverageNoteCount(int dvdCount);
    public void encode();
    public List<Note> decode();
}
