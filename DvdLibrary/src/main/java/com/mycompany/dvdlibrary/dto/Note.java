/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dto;

/**
 *
 * @author apprentice
 */
public class Note {
    
    private int noteId;
    private int dvdId;
    private String noteText;
    
    public Note() {
        
    }

    public int getNoteId() {
        return noteId;
    }

    public String getNoteText() {
        return noteText;
    }

    public int getDvdId() {
        return dvdId;
    }
    
    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public void setNoteText(String noteText) {
        this.noteText = noteText;
    } 

    public void setDvdId(int dvdId) {
        this.dvdId = dvdId;
    }    
}
