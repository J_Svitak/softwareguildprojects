/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dvdlibrary.dto;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Dvd {
    
    private int id;
    
    @NotEmpty(message = "DVD Title is Required!")
    private String title;
    
    @NotNull(message = "Release Date is Required!")
    private Date releaseDate;
    
    @NotEmpty(message = "Rating is Required!")
    private String rated;
    
    @NotEmpty(message = "Director is Required!")
    private String director;
    
    @NotEmpty(message = "Studio is Required!")
    private String studio;
    
    private List<Note> noteList;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public String getRated() {
        return rated;
    }

    public String getDirector() {
        return director;
    }

    public String getStudio() {
        return studio;
    }

    public List<Note> getNoteList() {
        return noteList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
    }    
}
