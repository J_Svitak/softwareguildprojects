/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $("#submitComment").on("click", function (e) {

        e.preventDefault();


        var commentData = JSON.stringify({
            comment: $("#comment").val(),
            blogPostId: $("#blogId").val()
        });

//        console.log(commentData);

        $.ajax({
            url: contextRoot + "/blogpost/c",
            type: "POST",
            data: commentData,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (data, status) {
                console.log(data);

                $('#btnCommentCollapse').modal('hide');
                comment: $("#comment").val("");

                var commentRow = buildCommentRow(data);
                $("#showComments").append($(commentRow));

//                $("#newPostTitle").val("");
//                $("#newCategory").val("");
//                $("#newPostBody").val("");
//                $("#newPostStartDate").val("");
//                $("#newPostExpirationDate").val("");
//                $("#newPostHashTag").val("");
            },
            error: function (data, status) {

//                $("#warnPostTitle").empty();
//                $("#warnPostCategory").empty();
//                $("#warnPostBody").empty();
//                $("#warnPostStartDate").empty();
//                $("#warnPostExpirationDate").empty();
//
//                var errors = data.responseJSON.errors;
//
//                $.each(errors, function (index, error) {
//
//                    console.log(error.fieldName + ": " + error.message);
//
//                    switch (error.fieldName) {
//                        case "title":
//                            $("#warnPostTitle").append(error.message);
//                            $("#newPostTitle").addClass("inputError");
//                            break;
//                        case "category":
//                            $("#warnPostCategory").append(error.message);
//                            $("#newCategory").addClass("inputError");
//                            break;
//                        case "body":
//                            $("#warnPostBody").append(error.message);
//                            $("#newPostBody").addClass("inputError");
//                            break;
//                        case "startDate":
//                            $("#warnPostStartDate").append(error.message);
//                            $("#newPostStartDate").addClass("inputError");
//                            break;
//                        case "expirationDate":
//                            $("#warnPostExpirationDate").append(error.message);
//                            $("#newPostExpirationDate").addClass("inputError");
//                            break;
//                        default:
//                            break;
//                    }
//                });
            }
        });
    });
    function buildCommentRow(data) {

        return "<tr id='category-row-" + data.id + "'>  \n\
                <td> " + data.userName + "</td>    \n\
                <td> " + data.comment + "</td>    \n\
                <td> " + data.commentDate + "</td>    \n\
                </tr>  ";
    }
});