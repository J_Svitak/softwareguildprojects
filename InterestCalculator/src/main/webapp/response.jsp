<%-- 
    Document   : response
    Created on : May 27, 2016, 11:24:32 AM
    Author     : apprentice
--%>

<%@taglib prefix="tl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>        
        <div id="container">
            <h1>Interest Calculator Results</h1>
            <div>
                <div>
                    Initial Principal Amount: ${principalAmount}
                </div>
                <div>
                    InterestRate: ${interestRate}
                </div>
                <div>
                    Compound Period: ${compoundPeriod}
                </div>
                <div>
                    Years the Fund is Open: ${fundLength}
                </div>
            </div>
            <hr/>
            <div>
                <table border="1">
                    <thead>
                        <tr>
                            <td>Year</td>
                            <td>Begin Balance</td>
                            <td>Yearly Interest</td>
                            <td>Ending Balance</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tl:forEach items="${yearlyInfo}" var="yearlyInfoElmt">
                            <tr>
                                <tl:forEach items="${yearlyInfoElmt}" var="element">
                                    <td>${element}</td>
                                </tl:forEach>
                            </tr>
                        </tl:forEach>
                    </tbody>                    
                </table>                
            </div>
        </div>
    </body>
</html>
