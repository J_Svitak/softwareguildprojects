<%-- 
    Document   : entry
    Created on : May 27, 2016, 11:24:21 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            div{
                width: 500px;
                text-align: right;
            }
            input {
                width: 120px;
            }
            select {
                width: 120px;
            }
            #container {
                text-align: center;
                width: 99%;
            }
        </style>
        
        <title>JSP Page</title>
    </head>
    <body>
        <form method="POST" action="InterestCalculatorServlet">
            <div id="container">
                <h1>Welcome to the Interest Calculator!</h1>
                <h3>Complete the fields below</h3>
                <div>
                    What is the annual interest rate?
                    <input type="text" name="interestRate"/>
                    ${errorInterestRate}
                </div>
                <div>
                    How often does the interest compound? 
                    <select name="compoundPeriod">
                        <option value=""></option>
                        <option value="QUARTERLY">QUARTERLY</option>
                        <option value="MONTHLY">MONTHLY</option>
                        <option value="DAILY">DAILY</option>
                    </select>
                    ${errorCompoundPeriod}
                </div>
                <div>
                    What is the initial Principal Amount?
                    <input type="text" name="principalAmount" />
                    ${errorPrincipalAmount}
                </div>
                <div>
                    How many years will the fund remain open?
                    <input type="text" name="fundLength" />
                    ${errorFundLength}
                </div>
                <div>
                    <input type="submit" value="Calculate" />
                </div>
            </div>
        </form>
    </body>
</html>
