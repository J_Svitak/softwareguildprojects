/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(urlPatterns = {"/InterestCalculatorServlet"})
public class InterestCalculatorServlet extends HttpServlet {

    final static int QUARTERLY_PERIOD_COUNT = 4;
    final static int MONTHLY_PERIOD_COUNT = 12;
    final static int DAILY_PERIOD_COUNT = 365;

    float totalAmount;
    float compoundInterestRate;
    int periodCount;
    int years;
    DecimalFormat currencyFormat = new DecimalFormat("#.##");
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        RequestDispatcher dispatchEntry = request.getRequestDispatcher("entry.jsp");
        dispatchEntry.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        float initialAmount = 0f;
        float interestRate = 0f;
        boolean isValid = true;
        
        try {
            interestRate = Float.parseFloat(request.getParameter("interestRate"));            
        }
        catch (NumberFormatException ex) {
            request.setAttribute("errorInterestRate", "Your entry was not a valid number!");            
            isValid = false;
        }
        
        String compoundPeriod = request.getParameter("compoundPeriod");        
        
        this.periodCount = getPeriodCount(compoundPeriod);
        this.compoundInterestRate = (float)interestRate / periodCount;
                
        this.totalAmount = 0;
        
        try {
            this.totalAmount = Float.parseFloat(request.getParameter("principalAmount"));
            initialAmount = getTotalAmount();
        }
        catch (NumberFormatException ex) {
            request.setAttribute("errorPrincipalAmount", "Your entry was not a valid number!");
            isValid = false;
        }
        
        this.years = 0;
        
        try {
            this.years = Integer.parseInt(request.getParameter("fundLength"));
        }
        catch (NumberFormatException ex) {
            request.setAttribute("errorFundLength", "Your entry was not a valid number!");
            isValid = false;
        }
        
        if (!isValid) {
            RequestDispatcher rdEntry = request.getRequestDispatcher("entry.jsp");
            rdEntry.forward(request, response);
        }
        
        generateYearlyGain(request, response);
        
        request.setAttribute("principalAmount", initialAmount);
        request.setAttribute("interestRate", interestRate);
        request.setAttribute("compoundPeriod", compoundPeriod);
        request.setAttribute("fundLength", getYears());        
        
        RequestDispatcher dispatchResponse = request.getRequestDispatcher("response.jsp");
        dispatchResponse.forward(request, response);
    }
    
    public static int getPeriodCount(String compoundPeriod) {
        //Determine the number of periods to compound interest
        int periodCount = 0;
        switch (compoundPeriod) {
            case "QUARTERLY":
                periodCount = QUARTERLY_PERIOD_COUNT;
                break;
            case "MONTHLY":
                periodCount = MONTHLY_PERIOD_COUNT;
                break;
            case "DAILY":
                periodCount = DAILY_PERIOD_COUNT;
                break;
            default:
                periodCount = 0;
        }
        return periodCount;
    }

    public void generateYearlyGain(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                       
        int years = getYears();                
        List<String[]> yearlyInfoList = new ArrayList();
        
        for (int i = 1; i <= years; i++) {
//                      
            String[] yearlyFundInfo = new String[4];

            yearlyFundInfo[0] = String.valueOf(i);
            yearlyFundInfo[1] = String.valueOf(getTotalAmount());
            
            //Calculate the yearly interest
            float yearlyInterest = 0.00f;
            float periodInterest = 0.00f;

            for (int p = 0; p < periodCount; p++) {
                //Calculate the interest for each period
                periodInterest = totalAmount * (getCompoundInterestRate() / 100);                
                yearlyInterest += periodInterest;
                this.totalAmount += periodInterest;
            }
            
            yearlyInterest = Float.parseFloat(currencyFormat.format(yearlyInterest));
            yearlyFundInfo[2] = String.valueOf(yearlyInterest);
            
            this.totalAmount = Float.parseFloat(currencyFormat.format(totalAmount));
            yearlyFundInfo[3] = String.valueOf(getTotalAmount());
            
            yearlyInfoList.add(yearlyFundInfo);
        }
        
        request.setAttribute("yearlyInfo", yearlyInfoList);

    }    
    
    public float getTotalAmount() {
        return this.totalAmount;
    }
    
    public float getCompoundInterestRate() {
        return this.compoundInterestRate;
    }
    
    public int getYears() {
        return this.years;
    }
    
    public String getName() {
        return "Interest Calculator";
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
