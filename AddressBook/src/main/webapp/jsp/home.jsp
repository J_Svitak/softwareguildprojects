<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <!--<link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">-->
        <link href="${pageContext.request.contextPath}/css/addressbook.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container col-md-12 cont-background">
            <div class="row">                
                <div class="col-md-12">                    
                    <h1 class="header" style="float: left">
                        <div class="col-md-1"><img src="${pageContext.request.contextPath}/img/icon.png" /></div>
                        <div class="col-md-10">Address Book Home</div>
                        <div class="col-md-1"><img src="${pageContext.request.contextPath}/img/icon.png" /></div
                    </h1>
                </div>                
            </div>
            <div class="navbar navbar-background">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}">Home</a></li>                 
                </ul>    
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row row-background">                
                    <div class="col-md-7" style="border: 1px solid #002D62; padding-bottom: 3px; height: 475px">
                        <h3 class="header">All Addresses</h3>
                        <div class="pageContent" style="height: 350px; overflow-y: scroll">
                            <table class="table table-bordered" id="addressTable">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${addressList}" var="address">
                                    <input type="hidden" id="addressId" value="${address.id}" />
                                        <tr id="addressRow-${address.id}">
                                            <td>${address.firstName}</td>
                                            <td><a data-addressId="${address.id}" data-toggle="modal" data-target="#showAddressModal">${address.lastName}</a></td>
                                            <td><a data-addressId="${address.id}" data-toggle="modal" data-target="#editAddressModal"><i class="glyphicon glyphicon-pencil"></i></a></td>
                                            <td><a data-addressId="${address.id}" class="delete-link">Delete<!--<i class="glyphicon glyphicon-remove"></i>--></a></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <form method="POST" class="form-horizontal">
                            <div class="pageContent" style="margin-top: 5px; padding: 5px">                                
                                <label for="filterValue">Search Value</label>
                                <input type="text" name="filterValue" id="filterValue" />
                                <input type="submit" value="Filter" id="submitFilter" />
                                <input type="submit" id="submitClearFilter" value="Display All Orders" class="btn btn-primary" />
                            </div>
                        </form>                        
                    </div>
                    <div class="col-md-5" style="border: 1px solid #002D62; height: 475px">                        
                        <h3 class="header" style="width:100%">Create New Address</h3>
                        <div class="pageContent" style="height: 400px; padding-top: 5px">
                            <form method="POST" class="form-horizontal">                            
                                <div class="form-group">
                                    <label for="txtFirstName" class="col-md-4 control-label">First Name:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txtFirstName" id="txtFirstName" />
                                    </div>                                    
                                </div>                        
                                <div class="form-group">
                                    <label for="txtLastName" class="col-md-4 control-label">Last Name:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txtLastName" id="txtLastName" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnLastName" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="txtStreetNumber" class="col-md-4 control-label">Street Number:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txtStreetNumber" id="txtStreetNumber" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnStreetNumber" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="txtStreetName" class="col-md-4 control-label">Street Name:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txtStreetName" id="txtStreetName" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnStreetName" class="col-md-8 warning"></div>
                                </div>                        
                                <div class="form-group">
                                    <label for="txtCity" class="col-md-4 control-label">City:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txtCity" id="txtCity" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnCity" class="col-md-8 warning"></div>
                                </div>
                                <div class="form-group">
                                    <label for="txtState" class="col-md-4 control-label">State:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txtState" id="txtState" />
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div id="warnState" class="col-md-8 warning"></div>
                                </div> 
                                <div class="form-group">
                                    <label for="txtZip" class="col-md-4 control-label">Zip:</label>
                                    <div class="col-md-8">
                                        <input type="text" name="txtZip" id="txtZip" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <input type="submit" value="Create Address" id="submitCreate" class="btn btn-primary"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="footer">
                Created by James Svitak
            </div>
        </div>            
        <div id="showAddressModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Address Summary</h4>
                    </div>
                    <div class="modal-body">
                        <div id="showModalSuccessHead" style="display:none">
                            <h6 style='color: green; font-weight: bold'>Address Successfully Created</h6>
                        </div>
                        <table class="table table-bordered" id="showAddressTable">
                            <tr>
                                <th>First Name:</th>
                                <td id="addressFirstName"></td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td id="addressLastName"></td>
                            </tr>
                            <tr>
                                <th>Street #</th>
                                <td id="addressStreetNumber"></td>
                            </tr>
                            <tr>
                                <th>Street Name:</th>
                                <td id="addressStreetName"></td>
                            </tr>
                            <tr>
                                <th>City:</th>
                                <td id="addressCity"></td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td id="addressState"></td>
                            </tr>
                            <tr>
                                <th>Zip:</th>
                                <td id="addressZip"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="editAddressModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Contact Details</h4>
                    </div>
                    <div class="modal-body">                  
                        <table class="table table-bordered" id="editAddressTable">
                            <input type="hidden" id="editAddressId" />
                            <tr>
                                <th>First Name:</th>
                                <td>
                                    <input type="text" id="editFirstName" class="form-control" />
                                </td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td>
                                    <div>
                                        <input type="text" id="editLastName" class="form-control" />
                                    </div>
                                    <div id="warnLastNameEdit" class="warning"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Street #:</th>
                                <td>
                                    <div>
                                        <input type="text" id="editStreetNumber" class="form-control" />
                                    </div>
                                    <div id="warnStreetNumberEdit" class="warning"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Street Name</th>
                                <td>
                                    <div>
                                        <input type="text" id="editStreetName" class="form-control" />
                                    </div>
                                    <div id="warnStreetNameEdit" class="warning"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>City:</th>
                                <td>
                                    <div>
                                        <input type="text" id="editCity" class="form-control" />
                                    </div>
                                    <div id="warnCityEdit" class="warning"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td>
                                    <div>
                                        <input type="text" id="editState" class="form-control" />
                                    </div>
                                    <div id="warnStateEdit" class="warning"></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Zip:</th>
                                <td> 
                                    <input type="text" id="editZip" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary" id="submitEdit">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="deleteAddressModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Address Summary</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <h6 style='color: red; font-weight: bold'>Permanently Delete Contact and Address?</h6>
                        </div>
                        <table class="table table-bordered" id="deleteAddressTable">
                            <tr>
                                <th>Address ID:</th>
                                <td id="deleteAddressId"></td>
                            </tr>
                            <tr>
                                <th>First Name:</th>
                                <td id="deleteFirstName"></td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td id="deleteLastName"></td>
                            </tr>                            
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <button type="button" class="btn btn-primary" id="submitDelete">Delete</button>
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            var contextRoot = "${pageContext.request.contextPath}";
        </script>
        
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/app.js"></script>

    </body>
</html>

