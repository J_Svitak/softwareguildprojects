/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    
    $("#submitCreate").on("click", function(e) {
       
        e.preventDefault();
        
        var addressData = JSON.stringify({           
           firstName: $("#txtFirstName").val(),
           lastName: $("#txtLastName").val(),
           streetNumber: $("#txtStreetNumber").val(),
           streetName: $("#txtStreetName").val(),
           city: $("#txtCity").val(),
           state: $("#txtState").val(),           
           zip: $("#txtZip").val()
        });
        
        $.ajax({
           url: contextRoot + "/address/",
           type: "POST",
           data: addressData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
                var addressRow = buildAddressRow(data);      
                $("#addressTable").append($(addressRow));

                $("#txtFirstName").val("");
                $("#txtLastName").val("");
                $("#txtStreetNumber").val("");
                $("#txtStreetName").val("");
                $("#txtCity").val("");
                $("#txtState").val("");
                $("#txtZip").val("");
                
                setValuesForShowModal(data);                                
                
                $("#showModalSuccessHead").css("display", "");                
                $("#showAddressModal").modal();
          },
          error: function(data, status) {
              
              $("#warnLastName").empty();
              $("#warnStreetNumber").empty();
              $("#warnStreetName").empty();
              $("#warnCity").empty();
              $("#warnState").empty();
              
              var errors = data.responseJSON.errors;
              
              $.each(errors, function(index, error) {
                  
                  console.log(error.fieldName + ": " + error.message);
                
                switch(error.fieldName) {
                    case "lastName":
                        $("#warnLastName").append(error.message);
                        $("#txtLastName").addClass("inputError");
                        break;
                    case "streetNumber":
                        $("#warnStreetNumber").append(error.message);
                        $("#txtStreetNumber").addClass("inputError");
                        break;
                    case "streetName":
                        $("#warnStreetName").append(error.message);
                        $("#txtStreetName").addClass("inputError");
                        break;
                    case "city":
                        $("#warnCity").append(error.message);
                        $("#txtCity").addClass("inputError");
                        break;
                    case "state":
                        $("#warnState").append(error.message);
                        $("#txtState").addClass("inputError");
                        break;
                    default:
                        break;
                }
              });              
          }
        });
    });
    
    $("#submitEdit").on("click", function(e) {
       
        e.preventDefault();
        
        var addressData = JSON.stringify({
           id: $("#editAddressId").val(),
           firstName: $("#editFirstName").val(),
           lastName: $("#editLastName").val(),
           streetNumber: $("#editStreetNumber").val(),
           streetName: $("#editStreetName").val(),
           city: $("#editCity").val(),
           state: $("#editState").val(),           
           zip: $("#editZip").val()
        });
        
        $.ajax({
           url: contextRoot + "/address/",
           type: "PUT",
           data: addressData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {
              console.log(data);
              
              $("#editAddressModal").modal("hide");
              
              var addressRow = buildAddressRow(data);
              $("#addressRow-" + data.id).replaceWith(addressRow);
          },
          error: function(data, status) {
              $("#warnLastNameEdit").empty();
              $("#warnStreetNumberEdit").empty();
              $("#warnStreetNameEdit").empty();
              $("#warnCityEdit").empty();
              $("#warnStateEdit").empty();
              
              var errors = data.responseJSON.errors;
              
              $.each(errors, function(index, error) {
                  
                console.log(error.fieldName + ": " + error.message);
                
                switch(error.fieldName) {
                    case "lastName":
                        $("#warnLastNameEdit").append(error.message);
                        $("#editLastName").addClass("inputError");
                        break;
                    case "streetNumber":
                        $("#warnStreetNumberEdit").append(error.message);
                        $("#editStreetNumber").addClass("inputError");
                        break;
                    case "streetName":
                        $("#warnStreetNameEdit").append(error.message);
                        $("#editStreetName").addClass("inputError");
                        break;
                    case "city":
                        $("#warnCityEdit").append(error.message);
                        $("#editCity").addClass("inputError");
                        break;
                    case "state":
                        $("#warnStateEdit").append(error.message);
                        $("#editState").addClass("inputError");
                        break;
                    default:
                        break;
                }
              });
          }
        });
    });
    
    $("#submitDelete").on("click", function(e) {
        
        var addressId = $("#deleteAddressId").text();
        console.log(addressId);
        
        $.ajax({
          type: "DELETE",
          url: contextRoot + "/address/" + addressId,
          success: function(data, status) {
              console.log(data);
              
              $("#orderRow-" + addressId).remove();
                $("#deleteAddressId").text("");
                $("#deleteFirstName").text("");
                $("#deleteLastName").text("");

                $("#deleteAddressModal").modal("hide");
          },
          error: function(data, status) {
              alert("There was an issue deleting contact " + addressId + "! \n"
                + status);
          }
      });
    });
    
    $("#submitFilter").on("click", function(e) {
        e.preventDefault();
        
        filterValue = $("#filterValue").val();
        
        if (filterValue == "") {
            return;
        }
        
        var addressData = JSON.stringify({            
           firstName: $("#txtFirstName").val(),
           lastName: $("#txtLastName").val(),
           streetNumber: $("#txtStreetNumber").val(),
           streetName: $("#txtStreetName").val(),
           city: $("#txtCity").val(),
           state: $("#txtState").val(),           
           zip: $("#txtZip").val()
        });
        
        console.log(addressData);
        
        $.ajax({
           url: contextRoot + "/address/filter/" + filterValue,
           type: "POST",
           data: addressData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {              
              
              $("#addressTable tbody").empty();
              
              for (var i = 0; i < data.length; i++) {
                  console.log(data[i]);                  
                  
                  var addressRow = buildAddressRow(data[i]);
                  $("#addressTable").append($(addressRow));
              }                
          },
          error: function(data, status) {
              alert("error");
          }
        });
    });
    
    $("#submitClearFilter").on("click", function(e) {
        e.preventDefault();
        
        var addressData = JSON.stringify({            
           firstName: $("#txtFirstName").val(),
           lastName: $("#txtLastName").val(),
           streetNumber: $("#txtStreetNumber").val(),
           streetName: $("#txtStreetName").val(),
           city: $("#txtCity").val(),
           state: $("#txtState").val(),           
           zip: $("#txtZip").val()
        });
        
        console.log(addressData);
        
        $.ajax({
           url: contextRoot + "/address/addresslist/",
           type: "POST",
           data: addressData,
           dataType: "json",
           beforeSend: function(xhr) {
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-type", "application/json");
          },
          success: function(data, status) {              
              
              $("#addressTable tbody").empty();
              
              for (var i = 0; i < data.length; i++) {
                  console.log(data[i]);                  
                  
                  var addressRow = buildAddressRow(data[i]);
                  $("#addressTable").append($(addressRow));
              }                
          },
          error: function(data, status) {
              alert("error");
          }
        });
    });
    
    $('#showAddressModal').on('show.bs.modal', function(e) {
       
       var link = $(e.relatedTarget);
       var addressId = link.data("addressid");
       
       $.ajax({
            url: contextRoot + "/address/" + addressId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                $("#showModalSuccessHead").css("display", "none");
                setValuesForShowModal(data);
            },
            error: function(data, status) {
                
            }            
       });
   });
   
    $('#editAddressModal').on('show.bs.modal', function(e) {
       
       var link = $(e.relatedTarget);
       var addressId = link.data("addressid");
       
       $.ajax({
            url: contextRoot + "/address/" + addressId,
            type: "GET",
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {

                $("#editAddressId").val(data.id);
                $("#editFirstName").val(data.firstName);
                $("#editLastName").val(data.lastName);
                $("#editStreetNumber").val(data.streetNumber);
                $("#editStreetName").val(data.streetName);
                $("#editCity").val(data.city);
                $("#editState").val(data.state);
                $("#editZip").val(data.zip);
            },
            error: function(data, status) {
                alert("error");
            }            
       });
   });
   
    $(document).on("click", ".delete-link", function(e) {
       
      e.preventDefault();      
      var addressId = $(e.target).data("addressid");
      console.log("id: " + addressId);
      $.ajax({
          type: "GET",
          url: contextRoot + "/address/" + addressId,
          dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            },
            success: function(data, status) {
                
                $("#deleteAddressId").text(data.id);
                $("#deleteFirstName").text(data.firstName);
                $("#deleteLastName").text(data.lastName);

                $("#deleteAddressModal").modal();
            },
            error: function(data, status) {
                
            }
//          
      });
   });
    
    function buildAddressRow(data) {
        return "<tr id='addressRow-" + data.id + "'>  \n\
                <td> " + data.firstName + "</td>    \n\
                <td><a data-addressId='" + data.id +"' data-toggle='modal' data-target='#showAddressModal'>" + data.lastName + "</a></td>  \n\
                <td> <a data-addressId='" + data.id +"' data-toggle='modal' data-target='#editAddressModal'><i class='glyphicon glyphicon-pencil'></i></a>  </td>   \n\
                <td> <a data-addressId='" + data.id +"' class='delete-link'><i class='glyphicon glyphicon-remove'></i></a>  </td>   \n\
                </tr>  ";
        }
        
    function setValuesForShowModal(data) {
        $("#addressFirstName").text(data.firstName);
        $("#addressLastName").text(data.lastName);
        $("#addressStreetNumber").text(data.streetNumber);
        $("#addressStreetName").text(data.streetName);
        $("#addressCity").text(data.city);
        $("#addressState").text(data.state);
        $("#addressZip").text(data.zip);
    }
});


