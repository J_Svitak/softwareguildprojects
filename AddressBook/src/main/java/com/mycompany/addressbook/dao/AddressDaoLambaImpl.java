/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.dao;

import com.mycompany.addressbook.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressDaoLambaImpl implements AddressDao {
    
    private int nextId = 101;
    private List<Address> addressList;
    private final String TOKEN = "::";
    private final String filename = "address_book.txt";
    
    public AddressDaoLambaImpl() {
        addressList = new ArrayList();
        decode();
    }
    
    @Override
    public Address create(Address objAddress) {
        
        objAddress.setId(nextId);
        nextId++;
        
        addressList.add(objAddress);
        encode();
        return objAddress;
    }
    
    @Override
    public Address getById(int addressId) {
        
        Address objAddress;
        return objAddress = addressList
                .stream()
                .filter(address -> address.getId() == addressId)
                .collect(Collectors.toList()).get(0);
    }
    
    @Override
    public List<Address> getByLastName(String lastName) {
                
        return addressList
                .stream()
                .filter(address -> address.getLastName().equals(lastName))
                .collect(Collectors.toList());
    }
    
    @Override
    public List<Address> getByFirstName(String firstName) {
                
        return addressList
                .stream()
                .filter(address -> address.getFirstName().equals(firstName))
                .collect(Collectors.toList());
    }
    
    @Override
    public List<Address> getByCity(String city) {
        
        return addressList
                .stream()
                .filter(address -> address.getLastName().equals(city))
                .collect(Collectors.toList());
    }
    
    @Override
    public Map<String, List<Address>> getByState(String state) {
        
        Map<String, List<Address>> stateAddressesMap = new HashMap();
        
        return stateAddressesMap =
                addressList
                .stream()
                .filter(address -> address.getState().equals(state))
                .collect(Collectors.groupingBy(Address::getCity));
    }
    
    @Override
    public List<Address> getByZip(String zip) {
        
        return addressList
                .stream()
                .filter(address -> address.getZip().equals(zip))
                .collect(Collectors.toList());
    }
    
    @Override
    public void update(Address objAddress) {
                
        Address addressToUpdate = addressList
                .stream()
                .filter(address -> address.getId() == objAddress.getId())
                .collect(Collectors.toList()).get(0);
                
        addressList.remove(addressToUpdate);
        addressList.add(objAddress);
        
        encode();
    }
    
    @Override
    public void delete(Address objAddress) {
        
        Address addressToDelete = addressList
                .stream()
                .filter(address -> address.getId() == objAddress.getId())
                .collect(Collectors.toList()).get(0);
        
        addressList.remove(addressToDelete);
                
        encode();
    }
    
    @Override
    public List<Address> getAddressList() {
        return addressList;
    }
    
    @Override
    public void encode() {
                
        try {            
            PrintWriter out = new PrintWriter(new FileWriter(filename));
            
            addressList
                    .stream()
                    .forEach((Address objAddress) -> {
                        out.print(objAddress.getId() + TOKEN);
                        out.print(objAddress.getFirstName() + TOKEN);
                        out.print(objAddress.getLastName() + TOKEN);
                        out.print(objAddress.getStreetNumber() + TOKEN);
                        out.print(objAddress.getStreetName() + TOKEN);
                        out.print(objAddress.getStreetName() + TOKEN);
                        out.print(objAddress.getCity() + TOKEN);
                        out.print(objAddress.getState() + TOKEN);
                        out.print(objAddress.getZip() + TOKEN);
                        out.print(objAddress.getCountry() + TOKEN);
                        out.println();
                        }
                    );
            
            out.flush();
            out.close();
        } 
        catch (IOException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    @Override
    public List<Address> decode() {
                
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(filename)));
            
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split(TOKEN);                
                
                Address objAddress = new Address();
                
                int id = Integer.parseInt(stringParts[0]);
                String firstName = stringParts[1];
                String lastName = stringParts[2];
                String streetNumber = stringParts[3];
                String streetName = stringParts[4];
                String city = stringParts[5];
                String state = stringParts[6];
                String zip = stringParts[7];
                String country = stringParts[8];
                
                objAddress.setId(id);
                objAddress.setFirstName(firstName);
                objAddress.setLastName(lastName);
                objAddress.setStreetNumber(streetNumber);
                objAddress.setStreetName(streetName);
                objAddress.setCity(city);
                objAddress.setState(state);
                objAddress.setZip(zip);
                objAddress.setCountry(country);                
                
                addressList.add(objAddress);
                
                if (id >= nextId) {
                    nextId = id + 1;
                }
            }
        } 
        catch (FileNotFoundException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return addressList;
    }    
}
