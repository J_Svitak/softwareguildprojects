/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.dao;

import com.mycompany.addressbook.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AddressDaoImpl implements AddressDao {
    
    private int nextId = 101;
    private List<Address> addressList;
    private final String filename = "address_book.txt";
    
    public AddressDaoImpl() {
        addressList = new ArrayList();
        decode();
    }
    
    @Override
    public Address getById(int addressId) {
                
        for (Address objAddress : addressList) {
            if (objAddress.getId() == addressId) {
                return objAddress;
            }
        }
        
        return null;
    }
    
    @Override
    public Address create(Address objAddress) {
        
        objAddress.setId(nextId);
        nextId++;
        
        addressList.add(objAddress);
        encode();
        return objAddress;
    }
    
    @Override
    public List<Address> getByLastName(String lastName) {
        
        List<Address> addressMatches = new ArrayList();
        for (Address objAddress : addressList) {
            if (lastName.equals(objAddress.getLastName())) {
                addressMatches.add(objAddress);
            }
        }
        
        return addressMatches;
    }
    
    @Override
    public List<Address> getByFirstName(String firstName) {
        
        List<Address> addressMatches = new ArrayList();
        for (Address objAddress : addressList) {
            if (firstName.equals(objAddress.getFirstName())) {
                addressMatches.add(objAddress);
            }
        }
        
        return addressMatches;
    }
    
    @Override
    public List<Address> getByCity(String city) {
        
        List<Address> addressMatches = new ArrayList();
        for (Address objAddress : addressList) {
            if (city.equals(objAddress.getCity())) {
                addressMatches.add(objAddress);
            }
        }
        
        return addressMatches;
    }
    
    @Override
    public Map<String, List<Address>> getByState(String state) {
        
        Map<String, List<Address>> stateAddressesMap = new HashMap();        
        
        for (Address objAddress : addressList) {
            if (state.equals(objAddress.getState())) {
                
                List<Address> cityAddressesList = new ArrayList();
                String city = objAddress.getCity();
                
                if (stateAddressesMap.containsKey(city))
                {
                    cityAddressesList = stateAddressesMap.get(city);
                }
                
                cityAddressesList.add(objAddress);
                stateAddressesMap.put(city, cityAddressesList);
            }
        }
        
        return stateAddressesMap;
    }
    
    @Override
    public List<Address> getByZip(String zip) {
        
        List<Address> addressMatches = new ArrayList();
        for (Address objAddress : addressList) {
            if (zip.equals(objAddress.getZip())) {
                addressMatches.add(objAddress);
            }
        }
        
        return addressMatches;
    }
    
    @Override
    public void update(Address objAddress) {
        
        for (Address myAddress : addressList) {
            if (myAddress.getId() == objAddress.getId()) {
                addressList.remove(myAddress);
                addressList.add(objAddress);
                break;
            }
        }
        
        encode();
    }
    
    @Override
    public void delete(Address objAddress) {
        for (Address myAddress : addressList) {
            if (myAddress.getId() == objAddress.getId()) {
                addressList.remove(myAddress);                
                break;
            }
        }
        
        encode();
    }
    
    @Override
    public List<Address> getAddressList() {
        return addressList;
    }
    
    @Override
    public void encode() {
        
        final String TOKEN = "::";
        
        try {            
            PrintWriter out = new PrintWriter(new FileWriter(filename));
            
            for (Address objAddress : addressList) {                
                
                out.print(objAddress.getId());
                out.print(TOKEN);
                
                out.print(objAddress.getFirstName());
                out.print(TOKEN);
                
                out.print(objAddress.getLastName());
                out.print(TOKEN);
                
                out.print(objAddress.getStreetNumber());
                out.print(TOKEN);
                
                out.print(objAddress.getStreetName());
                out.print(TOKEN);
                
                out.print(objAddress.getCity());
                out.print(TOKEN);
                
                out.print(objAddress.getState());
                out.print(TOKEN);
                
                out.print(objAddress.getZip());
                out.print(TOKEN);
                
                out.print(objAddress.getCountry());
                out.println();
            }            
            
            out.flush();
            out.close();
        } 
        catch (IOException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    @Override
    public List<Address> decode() {
                
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(filename)));
            
            while (sc.hasNextLine()) {
                String currentLine = sc.nextLine();
                
                String[] stringParts = currentLine.split("::");                
                
                Address objAddress = new Address();
                
                int id = Integer.parseInt(stringParts[0]);
                String firstName = stringParts[1];
                String lastName = stringParts[2];
                String streetNumber = stringParts[3];
                String streetName = stringParts[4];
                String city = stringParts[5];
                String state = stringParts[6];
                String zip = stringParts[7];
                String country = stringParts[8];
                
                objAddress.setId(id);
                objAddress.setFirstName(firstName);
                objAddress.setLastName(lastName);
                objAddress.setStreetNumber(streetNumber);
                objAddress.setStreetName(streetName);
                objAddress.setCity(city);
                objAddress.setState(state);
                objAddress.setZip(zip);
                objAddress.setCountry(country);
                
                
                
                addressList.add(objAddress);
                
                if (id >= nextId) {
                    nextId = id + 1;
                }
            }
        } 
        catch (FileNotFoundException ex) {
            //Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return addressList;
    }    
}
