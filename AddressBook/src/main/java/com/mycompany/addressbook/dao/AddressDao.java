/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.dao;

import com.mycompany.addressbook.dto.Address;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface AddressDao {
    
    public Address create(Address objAddress);
    public List<Address> getByLastName(String lastName);
    public List<Address> getByFirstName(String firstName);
    public Address getById(int addressId);
    public List<Address> getByCity(String city);
    public Map<String, List<Address>> getByState(String state);
    public List<Address> getByZip(String zip);
    public void update(Address objAddress);
    public void delete(Address objAddress);
    public List<Address> getAddressList();
    public void encode();
    public List<Address> decode();
    
}
