/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.dao;

import com.mycompany.addressbook.dto.Address;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AddressDaoDbImpl implements AddressDao {
    
    private List<Address> addressList;
    JdbcTemplate jdbcTemplate;
    
    private static final String SQL_GET_ADDRESS_LIST = "SELECT * "
            + "FROM person p "
            + "LEFT OUTER JOIN contactinfo ci ON ci.person_id = p.id "
            + "ORDER BY p.last_name, p.first_name";
    
    private static final String SQL_GET_ADDRESS_BY_LASTNAME = "SELECT * "
            + "FROM person p "
            + "LEFT OUTER JOIN contactinfo ci ON ci.person_id = p.id "
            + "WHERE last_name = ? "
            + "ORDER BY p.last_name, p.first_name";
    
    private static final String SQL_GET_ADDRESS_BY_ID = "SELECT * "
            + "FROM person p "
            + "LEFT OUTER JOIN contactinfo ci ON ci.person_id = p.id "
            + "WHERE p.id = ? ";            
    
    private static final String SQL_GET_ADDRESS_BY_FIRSTNAME = "SELECT * "
            + "FROM person p "
            + "LEFT OUTER JOIN contactinfo ci ON ci.person_id = p.id "
            + "WHERE first_name = ? "
            + "ORDER BY p.last_name, p.first_name";
    
    private static final String SQL_GET_ADDRESS_BY_CITY = "SELECT * "
            + "FROM person p "
            + "LEFT OUTER JOIN contactinfo ci ON ci.person_id = p.id "
            + "WHERE city = ? "
            + "ORDER BY p.last_name, p.first_name";
    
    private static final String SQL_GET_ADDRESS_BY_STATE = "SELECT * "
            + "FROM person p "
            + "LEFT OUTER JOIN contactinfo ci ON ci.person_id = p.id "
            + "WHERE state = ? "
            + "ORDER BY p.last_name, p.first_name";
    
    private static final String SQL_CREATE_ADDRESS_PERSON = "INSERT INTO person "
            + "(first_name, last_name) "
            + "VALUES(?, ?)";
    private static final String SQL_CREATE_ADDRESS_CONTACTINFO = "INSERT INTO contactinfo "
            + "(person_id, street_number, street_name, city, state, zip) "
            + "VALUES(?, ?, ?, ?, ?, ?)";
    
    private static final String SQL_UPDATE_ADDRESS_PERSON = "UPDATE person "
            + "SET first_name = ?, last_name = ? "
            + "WHERE id = ?";
    private static final String SQL_UPDATE_ADDRESS_CONTACTINFO = "UPDATE contactinfo "
            + "SET street_number = ?, street_name = ?, city = ?, state = ?, zip = ? "
            + "WHERE id = ?";
    
    private static final String SQL_DELETE_ADDRESS = "DELETE FROM person "            
            + "WHERE id = ?";
    private static final String SQL_DELETE_CONTACTINFO = "DELETE FROM contactinfo "            
            + "WHERE id = ?";
    
    
    public AddressDaoDbImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        addressList = getAddressList();        
    }    
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Address create(Address objAddress) {
        
        jdbcTemplate.update(SQL_CREATE_ADDRESS_PERSON,
                objAddress.getFirstName(),
                objAddress.getLastName()
        );
        
        Integer id = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        
        jdbcTemplate.update(SQL_CREATE_ADDRESS_CONTACTINFO,
                id,
                objAddress.getStreetNumber(),
                objAddress.getStreetName(),
                objAddress.getCity(),
                objAddress.getState(),
                objAddress.getZip()
        );
        
        objAddress.setId(id);
        return objAddress;
    }
    
    @Override
    public Address getById(int addressId) {
                
        return jdbcTemplate.queryForObject(SQL_GET_ADDRESS_BY_ID, new AddressMapper(), addressId);
    }
    
    @Override
    public List<Address> getByLastName(String lastName) {
        
        List<Address> addressMatches = new ArrayList();
        addressMatches = jdbcTemplate.query(SQL_GET_ADDRESS_BY_LASTNAME, new AddressMapper(), lastName);
        
        return addressMatches;
    }
    
    @Override
    public List<Address> getByFirstName(String firstName) {
        
        List<Address> addressMatches = new ArrayList();
        addressMatches = jdbcTemplate.query(SQL_GET_ADDRESS_BY_FIRSTNAME, new AddressMapper(), firstName);
        
        return addressMatches;
    }
    
    @Override
    public List<Address> getByCity(String city) {
        
        List<Address> addressMatches = new ArrayList();
        addressMatches = jdbcTemplate.query(SQL_GET_ADDRESS_BY_CITY, new AddressMapper(), city);
        
        return addressMatches;
    }
    
    @Override
    public Map<String, List<Address>> getByState(String state) {
        
        Map<String, List<Address>> stateAddressesMap = new HashMap();        
        
        for (Address objAddress : addressList) {
            if (state.equals(objAddress.getState())) {
                
                List<Address> cityAddressesList = new ArrayList();
                String city = objAddress.getCity();
                
                if (stateAddressesMap.containsKey(city))
                {
                    cityAddressesList = stateAddressesMap.get(city);
                }
                
                cityAddressesList.add(objAddress);
                stateAddressesMap.put(city, cityAddressesList);
            }
        }
        
        return stateAddressesMap;
    }
    
    @Override
    public List<Address> getByZip(String zip) {
        
        List<Address> addressMatches = new ArrayList();
        for (Address objAddress : addressList) {
            if (zip.equals(objAddress.getZip())) {
                addressMatches.add(objAddress);
            }
        }
        
        return addressMatches;
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(Address objAddress) {
        
        jdbcTemplate.update(SQL_UPDATE_ADDRESS_PERSON, 
                objAddress.getFirstName(),
                objAddress.getLastName(),
                objAddress.getId()
        );
        
        jdbcTemplate.update(SQL_UPDATE_ADDRESS_CONTACTINFO,
                objAddress.getStreetNumber(),
                objAddress.getStreetName(),
                objAddress.getCity(),
                objAddress.getState(),
                objAddress.getZip(),
                objAddress.getId()
        );
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Address objAddress) {
        
        jdbcTemplate.update(SQL_DELETE_CONTACTINFO, objAddress.getId());
        jdbcTemplate.update(SQL_DELETE_ADDRESS, objAddress.getId());        
        //addressList = getAddressList();
    }
    
    @Override
    public List<Address> getAddressList() {
        return jdbcTemplate.query(SQL_GET_ADDRESS_LIST, new AddressMapper());
    }    
    
    private static final class AddressMapper implements RowMapper<Address> {

        @Override
        public Address mapRow(ResultSet rs, int i) throws SQLException {
            
            Address address = new Address();
            
            address.setId(rs.getInt("id"));
            address.setFirstName(rs.getString("first_name"));
            address.setLastName(rs.getString("last_name"));
            address.setStreetNumber(rs.getString("street_number"));
            address.setStreetName(rs.getString("street_name"));
            address.setCity(rs.getString("city"));
            address.setState(rs.getString("state"));
            address.setZip(rs.getString("zip"));
            
            return address;
        }       
    }
    
    @Override
    public void encode() {        
        
    }
    
    @Override
    public List<Address> decode() {        
        
        return addressList;
    }
}
