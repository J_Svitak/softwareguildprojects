/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.dto;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author apprentice
 */
public class Address {
    
    private int id;
    private String firstName;
    
    @NotEmpty(message = "Last Name is Required")
    private String lastName;
    
    @NotEmpty(message = "Street Number is Required")
    private String streetNumber;
    
    @NotEmpty(message = "Street Name is Required")
    private String streetName;
    
    @NotEmpty(message = "City is Required")
    private String city;
    
    @NotEmpty(message = "State is Required")
    private String state;
    
    private String country;
    private String zip;    
    
    public Address() {
        
    }

    public int getId() {
        return id;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public String getStreetNumber() {
        return streetNumber;
    }
    
    public String getStreetName() {
        return streetName;
    }
        
    public String getCity() {
        return city;
    }
    
    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String getZip() {
        return zip;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }      
}
