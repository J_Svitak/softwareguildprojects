/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.controller;

import com.mycompany.addressbook.dao.AddressDao;
import com.mycompany.addressbook.dto.Address;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {
    
    private AddressDao addressDao;
    private List<Address> addressList = new ArrayList();
    
    @Inject
    public HomeController(AddressDao dao) {
        this.addressDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Map model) {
        addressList = addressDao.getAddressList();
        model.put("addressList", addressList);
        return "home";
    }    
}
