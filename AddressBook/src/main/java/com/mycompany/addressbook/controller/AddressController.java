/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.addressbook.controller;

import com.mycompany.addressbook.dao.AddressDao;
import com.mycompany.addressbook.dto.Address;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
@RequestMapping(value="/address")
public class AddressController {
    
    private AddressDao addressDao;
    private List<Address> addressList = new ArrayList();
    
    @Inject
    public AddressController(AddressDao dao) {
        this.addressDao = dao;
    }
    
    @RequestMapping(value="/", method=RequestMethod.POST)
    @ResponseBody
    public Address create(@Valid @RequestBody Address address) {
        
        return addressDao.create(address);        
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable("id") Integer addressId) {
        Address objAddress = addressDao.getById(addressId);
        addressDao.delete(objAddress);        
    }
    
    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public String edit(@PathVariable("id") Integer addressId, Map model) {
        
        Address objAddress = addressDao.getById(addressId);
        model.put("address", objAddress);
        return "edit";
    }
    
    @RequestMapping(value="/", method=RequestMethod.PUT)
    @ResponseBody
    public Address editSubmit(@Valid @RequestBody Address objAddress) {
        addressDao.update(objAddress);
        return objAddress;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Address show(@PathVariable("id") Integer addressId) {
        
        Address objAddress = addressDao.getById(addressId);        
        return objAddress;
    }
    
    @RequestMapping(value="/filter/{value}", method=RequestMethod.POST)
    @ResponseBody
    public List<Address> filterAddressList(@PathVariable("value") String filterValue) {
          
        addressList = new ArrayList();
        List<Address> returnList = new ArrayList();
        
        returnList = addressDao.getByLastName(filterValue);
        addressList = addToFilterList(returnList);
        
        returnList = addressDao.getByFirstName(filterValue);
        addressList = addToFilterList(returnList);
        
        returnList = addressDao.getByCity(filterValue);
        addressList = addToFilterList(returnList);
        
        returnList = addressDao.getByZip(filterValue);
        addressList = addToFilterList(returnList);            
        
        return addressList;
    }
    
    public List<Address> addToFilterList(List<Address> filterList) {
        for (Address address : filterList) {
            addressList.add(address);
        }
        
        return addressList;
    }
    
    @RequestMapping(value="/addresslist", method=RequestMethod.POST)
    @ResponseBody    
    public List<Address> clearFilter() {
        return addressDao.getAddressList();
    }
}
