/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(urlPatterns = {"/FactorizorServlet"})
public class FactorizorServlet extends HttpServlet {

    int userEntry;
    int factorSum;
    int factorCount;
        
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        
        RequestDispatcher dispatchEntry = request.getRequestDispatcher("entry.jsp");
        dispatchEntry.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        this.factorCount = 0;
        this.factorSum = 0;
        
        int num = 0;
        
        try {
            num = Integer.parseInt(request.getParameter("userNumber"));            
        }
        catch (NumberFormatException ex) {
            request.setAttribute("errorMessage", "Your entry was not a valid number!");
            RequestDispatcher rdEntry = request.getRequestDispatcher("entry.jsp");
            rdEntry.forward(request, response);
            return;
        }
        
        this.userEntry = num;
        request.setAttribute("userNumber", num);
        
        getFactorInfo(request, response);
        
        if (getFactorSum() == getUserEntry()) {
            request.setAttribute("isPerfect", true);
        }
       
        if (getFactorCount() == 1) {
            request.setAttribute("isPrime", true);
        }        
        
        RequestDispatcher dispatchResponse = request.getRequestDispatcher("response.jsp");
        dispatchResponse.forward(request, response);
    }
    
    public void getFactorInfo(HttpServletRequest request, HttpServletResponse response) {
        int num = getUserEntry();
        List<Integer> factorList = new ArrayList();
        
        for (int cntr = 1; cntr < num; cntr++) {            
            if (num % cntr == 0) {
                int currentFactorCount = getFactorCount();
                int currentFactorSum = getFactorSum();
                
                if (currentFactorCount == 0) {
                    request.setAttribute("factorsHeader", "The factors of %s are :" + num);                    
                }
                
                factorList.add(cntr);
                
                currentFactorCount++;
                this.factorCount = currentFactorCount;
                this.factorSum = currentFactorSum + cntr;
            }            
        }
        
        request.setAttribute("factorList", factorList);        
    }

    public void printFactorInfo() {
        if (getFactorSum() == getUserEntry()) {
            System.out.println();
            System.out.println(String.format("%s is a perfect number", getUserEntry()));
        }
       
        if (getFactorCount() == 1) {
            System.out.println();
            System.out.println(String.format("%s is a prime number.", getUserEntry()));
        }        
    }
        
    public int getUserEntry() {
        return this.userEntry;
    }
    
    public int getFactorSum() {
        return this.factorSum;
    }
    
    public int getFactorCount() {
        return this.factorCount;
    }
    
    public String getName() {
        return "Factorizer";
    }    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
