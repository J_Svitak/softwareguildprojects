<%-- 
    Document   : response
    Created on : May 27, 2016, 10:39:15 AM
    Author     : apprentice
--%>

<%@taglib prefix="tl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div>
            <a href="http://localhost:8080/Factorizor/">Factorizor Home</a>
        </div>
        <h1>Factor Results</h1>
        <h3>${factorsHeader}</h3>
        <tl:forEach items="${factorList}" var="factor">
            ${factor} <br/>
        </tl:forEach>
        <br/>
        <tl:if test="${isPerfect}">
            ${userNumber} is a Perfect Number...
            <br/>
        </tl:if> 
        <tl:if test="${isPrime}">
            ${userNumber} is a Prime Number...
            <br/>
        </tl:if>         
    </body>
</html>
