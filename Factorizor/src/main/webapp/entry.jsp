<%-- 
    Document   : entry
    Created on : May 27, 2016, 10:33:15 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form method="post" action="FactorizorServlet">
            <h1>Welcome to the Factorizor!</h1>
            Enter any number to see its factors <br/>
            <input type="text" name="userNumber" />
            <input type="submit" value="Start Game" /> <br/>
            <br/>
            <h3>${errorMessage}</h3>
        </form>
    </body>
</html>
